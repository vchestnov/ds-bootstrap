(********************************************************************)
(*                                                                  *)
(*  Ancillary file to                                               *)
(*  "Hexagon Bootstrap in the Double Scaling Limit"                 *)
(*  by V.Chestnov and G. Papathanasiou.                             *)
(*  arXiv:2012.XXXXX                                                *)
(*                                                                  *)
(*  This file contains the (1111) component of the NMHV six gluon   *)
(*  amplitude in the combined DS and origin limit represented by    *)
(*  the `EO` variable                                               *)
(********************************************************************)
EOfull = 1 + g^2*(-Log[u]^2/2 + Log[u]*Log[v] - Log[v]^2/2 - Log[w]^2/2 - 
      2*zeta[2]) + g^4*(Log[u]^4/8 - (Log[u]^3*Log[v])/2 + 
      (Log[u]^2*Log[v]^2)/2 - (Log[u]*Log[v]^3)/2 + Log[v]^4/8 + 
      (Log[u]^2/4 - (Log[u]*Log[v])/2 + Log[v]^2/4)*Log[w]^2 + Log[w]^4/8 + 
      ((3*Log[u]^2)/2 - 5*Log[u]*Log[v] + (3*Log[v]^2)/2 + 2*Log[w]^2)*
       zeta[2] + (39*zeta[2]^2)/10 + (Log[u] + Log[v])*zeta[3]) + 
    g^6*(-Log[u]^6/48 + (Log[u]^5*Log[v])/8 - (3*Log[u]^4*Log[v]^2)/16 + 
      (5*Log[u]^3*Log[v]^3)/18 - (3*Log[u]^2*Log[v]^4)/16 + 
      (Log[u]*Log[v]^5)/8 - Log[v]^6/48 + 
      (-Log[u]^4/16 + (Log[u]^3*Log[v])/4 - (Log[u]^2*Log[v]^2)/4 + 
        (Log[u]*Log[v]^3)/4 - Log[v]^4/16)*Log[w]^2 + 
      (-Log[u]^2/16 + (Log[u]*Log[v])/8 - Log[v]^2/16)*Log[w]^4 - 
      Log[w]^6/48 + ((-69*Log[u]^2)/20 + (271*Log[u]*Log[v])/10 - 
        (69*Log[v]^2)/20 + (2*Log[u] + 2*Log[v])*Log[w] - (167*Log[w]^2)/20)*
       zeta[2]^2 - (527*zeta[2]^3)/105 + (-Log[u]^3 - Log[v]^3 + 
        (Log[u]^2/2 + Log[v]^2/2)*Log[w] + (-Log[u]/2 - Log[v]/2)*Log[w]^2)*
       zeta[3] + zeta[2]*(-Log[u]^4/2 + (19*Log[u]^3*Log[v])/6 - 
        (5*Log[u]^2*Log[v]^2)/2 + (19*Log[u]*Log[v]^3)/6 - Log[v]^4/2 + 
        ((Log[u]^2*Log[v])/2 + (Log[u]*Log[v]^2)/2)*Log[w] + 
        ((-5*Log[u]^2)/4 + (7*Log[u]*Log[v])/2 - (5*Log[v]^2)/4)*Log[w]^2 - 
        (3*Log[w]^4)/4 + (-6*Log[u] - 6*Log[v])*zeta[3]) + 
      (-8*Log[u] - 8*Log[v])*zeta[5]) + 
    g^8*(Log[u]^8/384 - (Log[u]^7*Log[v])/48 + (Log[u]^6*Log[v]^2)/24 - 
      (11*Log[u]^5*Log[v]^3)/144 + (23*Log[u]^4*Log[v]^4)/288 - 
      (11*Log[u]^3*Log[v]^5)/144 + (Log[u]^2*Log[v]^6)/24 - 
      (Log[u]*Log[v]^7)/48 + Log[v]^8/384 + 
      (Log[u]^6/96 - (Log[u]^5*Log[v])/16 + (3*Log[u]^4*Log[v]^2)/32 - 
        (5*Log[u]^3*Log[v]^3)/36 + (3*Log[u]^2*Log[v]^4)/32 - 
        (Log[u]*Log[v]^5)/16 + Log[v]^6/96)*Log[w]^2 + 
      (Log[u]^4/64 - (Log[u]^3*Log[v])/16 + (Log[u]^2*Log[v]^2)/16 - 
        (Log[u]*Log[v]^3)/16 + Log[v]^4/64)*Log[w]^4 + 
      (Log[u]^2/96 - (Log[u]*Log[v])/48 + Log[v]^2/96)*Log[w]^6 + 
      Log[w]^8/384 + ((-211*Log[u]^2)/210 - (36289*Log[u]*Log[v])/210 - 
        (211*Log[v]^2)/210 + ((-147*Log[u])/5 - (147*Log[v])/5)*Log[w] + 
        (803*Log[w]^2)/21)*zeta[2]^3 - (181451*zeta[2]^4)/4200 + 
      ((17*Log[u]^2)/4 - 9*Log[u]*Log[v] + (17*Log[v]^2)/4 + 3*Log[w]^2)*
       zeta[3]^2 + zeta[2]^2*((77*Log[u]^4)/60 - (1163*Log[u]^3*Log[v])/60 + 
        (227*Log[u]^2*Log[v]^2)/20 - (1163*Log[u]*Log[v]^3)/60 + 
        (77*Log[v]^4)/60 + ((-37*Log[u]^3)/30 - (67*Log[u]^2*Log[v])/10 - 
          (67*Log[u]*Log[v]^2)/10 - (37*Log[v]^3)/30)*Log[w] + 
        ((207*Log[u]^2)/40 - (479*Log[u]*Log[v])/20 + (207*Log[v]^2)/40)*
         Log[w]^2 + (-Log[u] - Log[v])*Log[w]^3 + (67*Log[w]^4)/16 + 
        ((319*Log[u])/10 + (319*Log[v])/10 + (14*Log[w])/5)*zeta[3]) + 
      ((55*Log[u]^3)/6 + (55*Log[v]^3)/6 + (-5*Log[u]^2 - 5*Log[v]^2)*
         Log[w] + (4*Log[u] + 4*Log[v])*Log[w]^2)*zeta[5] + 
      zeta[3]*((3*Log[u]^5)/8 - (7*Log[u]^4*Log[v])/24 + 
        (Log[u]^3*Log[v]^2)/3 + (Log[u]^2*Log[v]^3)/3 - 
        (7*Log[u]*Log[v]^4)/24 + (3*Log[v]^5)/8 + 
        (-Log[u]^4/4 + (Log[u]^3*Log[v])/6 - (Log[u]^2*Log[v]^2)/2 + 
          (Log[u]*Log[v]^3)/6 - Log[v]^4/4)*Log[w] + 
        (Log[u]^3/2 + Log[v]^3/2)*Log[w]^2 + (-Log[u]^2/4 - Log[v]^2/4)*
         Log[w]^3 + (Log[u]/8 + Log[v]/8)*Log[w]^4 + 4*zeta[5]) + 
      zeta[2]*((5*Log[u]^6)/48 - (23*Log[u]^5*Log[v])/24 + 
        (5*Log[u]^4*Log[v]^2)/4 - (37*Log[u]^3*Log[v]^3)/18 + 
        (5*Log[u]^2*Log[v]^4)/4 - (23*Log[u]*Log[v]^5)/24 + (5*Log[v]^6)/48 + 
        (-(Log[u]^4*Log[v])/4 - (Log[u]^3*Log[v]^2)/12 - (Log[u]^2*Log[v]^3)/
           12 - (Log[u]*Log[v]^4)/4)*Log[w] + 
        ((3*Log[u]^4)/8 - (25*Log[u]^3*Log[v])/12 + (7*Log[u]^2*Log[v]^2)/4 - 
          (25*Log[u]*Log[v]^3)/12 + (3*Log[v]^4)/8)*Log[w]^2 + 
        (-(Log[u]^2*Log[v])/4 - (Log[u]*Log[v]^2)/4)*Log[w]^3 + 
        ((7*Log[u]^2)/16 - (9*Log[u]*Log[v])/8 + (7*Log[v]^2)/16)*Log[w]^4 + 
        Log[w]^6/6 + ((43*Log[u]^3)/6 + (43*Log[v]^3)/6 + 
          (-2*Log[u]^2 - 2*Log[v]^2)*Log[w] + (3*Log[u] + 3*Log[v])*Log[w]^2)*
         zeta[3] + 14*zeta[3]^2 + (53*Log[u] + 53*Log[v] - 4*Log[w])*
         zeta[5]) + (75*Log[u] + 75*Log[v])*zeta[7]) + 
    g^10*(-Log[u]^10/3840 + (Log[u]^9*Log[v])/384 - (5*Log[u]^8*Log[v]^2)/
       768 + (Log[u]^7*Log[v]^3)/72 - (11*Log[u]^6*Log[v]^4)/576 + 
      (163*Log[u]^5*Log[v]^5)/7200 - (11*Log[u]^4*Log[v]^6)/576 + 
      (Log[u]^3*Log[v]^7)/72 - (5*Log[u]^2*Log[v]^8)/768 + 
      (Log[u]*Log[v]^9)/384 - Log[v]^10/3840 + 
      (-Log[u]^8/768 + (Log[u]^7*Log[v])/96 - (Log[u]^6*Log[v]^2)/48 + 
        (11*Log[u]^5*Log[v]^3)/288 - (23*Log[u]^4*Log[v]^4)/576 + 
        (11*Log[u]^3*Log[v]^5)/288 - (Log[u]^2*Log[v]^6)/48 + 
        (Log[u]*Log[v]^7)/96 - Log[v]^8/768)*Log[w]^2 + 
      (-Log[u]^6/384 + (Log[u]^5*Log[v])/64 - (3*Log[u]^4*Log[v]^2)/128 + 
        (5*Log[u]^3*Log[v]^3)/144 - (3*Log[u]^2*Log[v]^4)/128 + 
        (Log[u]*Log[v]^5)/64 - Log[v]^6/384)*Log[w]^4 + 
      (-Log[u]^4/384 + (Log[u]^3*Log[v])/96 - (Log[u]^2*Log[v]^2)/96 + 
        (Log[u]*Log[v]^3)/96 - Log[v]^4/384)*Log[w]^6 + 
      (-Log[u]^2/768 + (Log[u]*Log[v])/384 - Log[v]^2/768)*Log[w]^8 - 
      Log[w]^10/3840 + ((372161*Log[u]^2)/2800 + (5159389*Log[u]*Log[v])/
         4200 + (372161*Log[v]^2)/2800 + ((16109*Log[u])/50 + 
          (16109*Log[v])/50)*Log[w] - (209459*Log[w]^2)/1200)*zeta[2]^4 + 
      (5596791*zeta[2]^5)/7700 + ((-7*Log[u]^4)/3 + (89*Log[u]^3*Log[v])/12 - 
        (37*Log[u]^2*Log[v]^2)/4 + (89*Log[u]*Log[v]^3)/12 - (7*Log[v]^4)/3 + 
        (Log[u]^3/6 - (3*Log[u]^2*Log[v])/2 - (3*Log[u]*Log[v]^2)/2 + 
          Log[v]^3/6)*Log[w] + ((-29*Log[u]^2)/8 + (15*Log[u]*Log[v])/2 - 
          (29*Log[v]^2)/8)*Log[w]^2 - (3*Log[w]^4)/2)*zeta[3]^2 + 
      (-9*Log[u] - 9*Log[v] + 9*Log[w])*zeta[3]^3 + 
      zeta[2]^3*((639*Log[u]^4)/280 + (54637*Log[u]^3*Log[v])/420 - 
        (5662*Log[u]^2*Log[v]^2)/105 + (54637*Log[u]*Log[v]^3)/420 + 
        (639*Log[v]^4)/280 + ((583*Log[u]^3)/28 + (1903*Log[u]^2*Log[v])/28 + 
          (1903*Log[u]*Log[v]^2)/28 + (583*Log[v]^3)/28)*Log[w] + 
        ((-3289*Log[u]^2)/210 + (74899*Log[u]*Log[v])/420 - 
          (3289*Log[v]^2)/210)*Log[w]^2 + ((86*Log[u])/5 + (86*Log[v])/5)*
         Log[w]^3 - (2867*Log[w]^4)/120 + ((-3838*Log[u])/21 - 
          (3838*Log[v])/21 - (1159*Log[w])/35)*zeta[3]) + 
      ((-29*Log[u]^5)/8 + (65*Log[u]^4*Log[v])/24 - (13*Log[u]^3*Log[v]^2)/
         4 - (13*Log[u]^2*Log[v]^3)/4 + (65*Log[u]*Log[v]^4)/24 - 
        (29*Log[v]^5)/8 + ((61*Log[u]^4)/24 - (5*Log[u]^3*Log[v])/3 + 
          5*Log[u]^2*Log[v]^2 - (5*Log[u]*Log[v]^3)/3 + (61*Log[v]^4)/24)*
         Log[w] + ((-55*Log[u]^3)/12 - (55*Log[v]^3)/12)*Log[w]^2 + 
        ((5*Log[u]^2)/2 + (5*Log[v]^2)/2)*Log[w]^3 + (-Log[u] - Log[v])*
         Log[w]^4)*zeta[5] - 32*zeta[5]^2 + 
      zeta[2]^2*((-7*Log[u]^6)/24 + (3901*Log[u]^5*Log[v])/600 - 
        (581*Log[u]^4*Log[v]^2)/80 + (161*Log[u]^3*Log[v]^3)/12 - 
        (581*Log[u]^2*Log[v]^4)/80 + (3901*Log[u]*Log[v]^5)/600 - 
        (7*Log[v]^6)/24 + ((11*Log[u]^5)/30 + (27*Log[u]^4*Log[v])/8 + 
          (47*Log[u]^3*Log[v]^2)/30 + (47*Log[u]^2*Log[v]^3)/30 + 
          (27*Log[u]*Log[v]^4)/8 + (11*Log[v]^5)/30)*Log[w] + 
        ((-47*Log[u]^4)/30 + (1877*Log[u]^3*Log[v])/120 - 
          (81*Log[u]^2*Log[v]^2)/8 + (1877*Log[u]*Log[v]^3)/120 - 
          (47*Log[v]^4)/30)*Log[w]^2 + ((37*Log[u]^3)/60 + 
          (77*Log[u]^2*Log[v])/20 + (77*Log[u]*Log[v]^2)/20 + 
          (37*Log[v]^3)/60)*Log[w]^3 + ((-77*Log[u]^2)/32 + 
          (727*Log[u]*Log[v])/80 - (77*Log[v]^2)/32)*Log[w]^4 + 
        (Log[u]/4 + Log[v]/4)*Log[w]^5 - (181*Log[w]^6)/160 + 
        ((-533*Log[u]^3)/12 + (7*Log[u]^2*Log[v])/4 + (7*Log[u]*Log[v]^2)/4 - 
          (533*Log[v]^3)/12 + ((31*Log[u]^2)/10 + 2*Log[u]*Log[v] + 
            (31*Log[v]^2)/10)*Log[w] + ((-307*Log[u])/20 - (307*Log[v])/20)*
           Log[w]^2 - (2*Log[w]^3)/5)*zeta[3] - 100*zeta[3]^2 + 
        ((-1501*Log[u])/5 - (1501*Log[v])/5 + (21*Log[w])/5)*zeta[5]) + 
      zeta[3]*(-Log[u]^7/12 + (Log[u]^6*Log[v])/8 - (3*Log[u]^5*Log[v]^2)/
         16 + (7*Log[u]^4*Log[v]^3)/144 + (7*Log[u]^3*Log[v]^4)/144 - 
        (3*Log[u]^2*Log[v]^5)/16 + (Log[u]*Log[v]^6)/8 - Log[v]^7/12 + 
        (Log[u]^6/16 - (Log[u]^5*Log[v])/12 + (5*Log[u]^4*Log[v]^2)/24 - 
          (Log[u]^3*Log[v]^3)/6 + (5*Log[u]^2*Log[v]^4)/24 - 
          (Log[u]*Log[v]^5)/12 + Log[v]^6/16)*Log[w] + 
        ((-3*Log[u]^5)/16 + (7*Log[u]^4*Log[v])/48 - (Log[u]^3*Log[v]^2)/6 - 
          (Log[u]^2*Log[v]^3)/6 + (7*Log[u]*Log[v]^4)/48 - (3*Log[v]^5)/16)*
         Log[w]^2 + (Log[u]^4/8 - (Log[u]^3*Log[v])/12 + (Log[u]^2*Log[v]^2)/
           4 - (Log[u]*Log[v]^3)/12 + Log[v]^4/8)*Log[w]^3 + 
        (-Log[u]^3/8 - Log[v]^3/8)*Log[w]^4 + (Log[u]^2/16 + Log[v]^2/16)*
         Log[w]^5 + (-Log[u]/48 - Log[v]/48)*Log[w]^6 + 
        (-90*Log[u]^2 + 182*Log[u]*Log[v] - 90*Log[v]^2 + 
          (3*Log[u] + 3*Log[v])*Log[w] - 62*Log[w]^2)*zeta[5] - 60*zeta[7]) + 
      (-93*Log[u]^3 - 93*Log[v]^3 + ((105*Log[u]^2)/2 + (105*Log[v]^2)/2)*
         Log[w] + ((-75*Log[u])/2 - (75*Log[v])/2)*Log[w]^2)*zeta[7] + 
      zeta[2]*(-Log[u]^8/64 + (3*Log[u]^7*Log[v])/16 - (11*Log[u]^6*Log[v]^2)/
         32 + (157*Log[u]^5*Log[v]^3)/240 - (95*Log[u]^4*Log[v]^4)/144 + 
        (157*Log[u]^3*Log[v]^5)/240 - (11*Log[u]^2*Log[v]^6)/32 + 
        (3*Log[u]*Log[v]^7)/16 - Log[v]^8/64 + 
        ((Log[u]^6*Log[v])/16 - (Log[u]^5*Log[v]^2)/48 + (Log[u]^4*Log[v]^3)/
           16 + (Log[u]^3*Log[v]^4)/16 - (Log[u]^2*Log[v]^5)/48 + 
          (Log[u]*Log[v]^6)/16)*Log[w] + ((-7*Log[u]^6)/96 + 
          (29*Log[u]^5*Log[v])/48 - (13*Log[u]^4*Log[v]^2)/16 + 
          (47*Log[u]^3*Log[v]^3)/36 - (13*Log[u]^2*Log[v]^4)/16 + 
          (29*Log[u]*Log[v]^5)/48 - (7*Log[v]^6)/96)*Log[w]^2 + 
        ((Log[u]^4*Log[v])/8 + (Log[u]^3*Log[v]^2)/24 + (Log[u]^2*Log[v]^3)/
           24 + (Log[u]*Log[v]^4)/8)*Log[w]^3 + 
        (-Log[u]^4/8 + (31*Log[u]^3*Log[v])/48 - (9*Log[u]^2*Log[v]^2)/16 + 
          (31*Log[u]*Log[v]^3)/48 - Log[v]^4/8)*Log[w]^4 + 
        ((Log[u]^2*Log[v])/16 + (Log[u]*Log[v]^2)/16)*Log[w]^5 + 
        ((-3*Log[u]^2)/32 + (11*Log[u]*Log[v])/48 - (3*Log[v]^2)/32)*
         Log[w]^6 - (5*Log[w]^8)/192 + ((-65*Log[u]^2)/2 + 61*Log[u]*Log[v] - 
          (65*Log[v]^2)/2 + ((-9*Log[u])/2 - (9*Log[v])/2)*Log[w] - 
          25*Log[w]^2)*zeta[3]^2 + zeta[3]*((-77*Log[u]^5)/24 + 
          (59*Log[u]^4*Log[v])/24 - (31*Log[u]^3*Log[v]^2)/12 - 
          (31*Log[u]^2*Log[v]^3)/12 + (59*Log[u]*Log[v]^4)/24 - 
          (77*Log[v]^5)/24 + ((25*Log[u]^4)/24 - Log[u]^3*Log[v] + 
            3*Log[u]^2*Log[v]^2 - Log[u]*Log[v]^3 + (25*Log[v]^4)/24)*
           Log[w] + ((-15*Log[u]^3)/4 + (Log[u]^2*Log[v])/2 + 
            (Log[u]*Log[v]^2)/2 - (15*Log[v]^3)/4)*Log[w]^2 + 
          ((3*Log[u]^2)/2 + (3*Log[v]^2)/2)*Log[w]^3 + 
          ((-3*Log[u])/4 - (3*Log[v])/4)*Log[w]^4 - 296*zeta[5]) + 
        ((-413*Log[u]^3)/6 - (3*Log[u]^2*Log[v])/2 - (3*Log[u]*Log[v]^2)/2 - 
          (413*Log[v]^3)/6 + (23*Log[u]^2 + 23*Log[v]^2)*Log[w] + 
          ((-49*Log[u])/2 - (49*Log[v])/2)*Log[w]^2 + 2*Log[w]^3)*zeta[5] + 
        (-528*Log[u] - 528*Log[v] + 60*Log[w])*zeta[7]) + 
      (-784*Log[u] - 784*Log[v])*zeta[9]) + 
    g^12*(Log[u]^12/46080 - (Log[u]^11*Log[v])/3840 + 
      (Log[u]^10*Log[v]^2)/1280 - (13*Log[u]^9*Log[v]^3)/6912 + 
      (29*Log[u]^8*Log[v]^4)/9216 - (7*Log[u]^7*Log[v]^5)/1600 + 
      (619*Log[u]^6*Log[v]^6)/129600 - (7*Log[u]^5*Log[v]^7)/1600 + 
      (29*Log[u]^4*Log[v]^8)/9216 - (13*Log[u]^3*Log[v]^9)/6912 + 
      (Log[u]^2*Log[v]^10)/1280 - (Log[u]*Log[v]^11)/3840 + Log[v]^12/46080 + 
      (Log[u]^10/7680 - (Log[u]^9*Log[v])/768 + (5*Log[u]^8*Log[v]^2)/1536 - 
        (Log[u]^7*Log[v]^3)/144 + (11*Log[u]^6*Log[v]^4)/1152 - 
        (163*Log[u]^5*Log[v]^5)/14400 + (11*Log[u]^4*Log[v]^6)/1152 - 
        (Log[u]^3*Log[v]^7)/144 + (5*Log[u]^2*Log[v]^8)/1536 - 
        (Log[u]*Log[v]^9)/768 + Log[v]^10/7680)*Log[w]^2 + 
      (Log[u]^8/3072 - (Log[u]^7*Log[v])/384 + (Log[u]^6*Log[v]^2)/192 - 
        (11*Log[u]^5*Log[v]^3)/1152 + (23*Log[u]^4*Log[v]^4)/2304 - 
        (11*Log[u]^3*Log[v]^5)/1152 + (Log[u]^2*Log[v]^6)/192 - 
        (Log[u]*Log[v]^7)/384 + Log[v]^8/3072)*Log[w]^4 + 
      (Log[u]^6/2304 - (Log[u]^5*Log[v])/384 + (Log[u]^4*Log[v]^2)/256 - 
        (5*Log[u]^3*Log[v]^3)/864 + (Log[u]^2*Log[v]^4)/256 - 
        (Log[u]*Log[v]^5)/384 + Log[v]^6/2304)*Log[w]^6 + 
      (Log[u]^4/3072 - (Log[u]^3*Log[v])/768 + (Log[u]^2*Log[v]^2)/768 - 
        (Log[u]*Log[v]^3)/768 + Log[v]^4/3072)*Log[w]^8 + 
      (Log[u]^2/7680 - (Log[u]*Log[v])/3840 + Log[v]^2/7680)*Log[w]^10 + 
      Log[w]^12/46080 + ((-54510263*Log[u]^2)/30800 - 
        (431611303*Log[u]*Log[v])/46200 - (54510263*Log[v]^2)/30800 + 
        ((-1687172*Log[u])/525 - (1687172*Log[v])/525)*Log[w] + 
        (15495527*Log[w]^2)/23100)*zeta[2]^5 - (204234195041*zeta[2]^6)/
       25225200 + ((529*Log[u]^3)/36 - (19*Log[u]^2*Log[v])/4 - 
        (19*Log[u]*Log[v]^2)/4 + (529*Log[v]^3)/36 + 
        (-16*Log[u]^2 + 7*Log[u]*Log[v] - 16*Log[v]^2)*Log[w] + 
        ((19*Log[u])/2 + (19*Log[v])/2)*Log[w]^2 - (9*Log[w]^3)/2)*
       zeta[3]^3 - (5*zeta[3]^4)/4 + zeta[2]^4*((-968131*Log[u]^4)/11200 - 
        (4750651*Log[u]^3*Log[v])/5040 + (2198377*Log[u]^2*Log[v]^2)/8400 - 
        (4750651*Log[u]*Log[v]^3)/5040 - (968131*Log[v]^4)/11200 + 
        ((-75431*Log[u]^3)/300 - (191219*Log[u]^2*Log[v])/300 - 
          (191219*Log[u]*Log[v]^2)/300 - (75431*Log[v]^3)/300)*Log[w] + 
        ((-102583*Log[u]^2)/3360 - (11879549*Log[u]*Log[v])/8400 - 
          (102583*Log[v]^2)/3360)*Log[w]^2 + ((-63527*Log[u])/300 - 
          (63527*Log[v])/300)*Log[w]^3 + (304379*Log[w]^4)/2240 + 
        ((1594003*Log[u])/1400 + (1594003*Log[v])/1400 + (52224*Log[w])/175)*
         zeta[3]) + (482*Log[u]^2 - 931*Log[u]*Log[v] + 482*Log[v]^2 + 
        (-30*Log[u] - 30*Log[v])*Log[w] + 321*Log[w]^2)*zeta[5]^2 + 
      zeta[3]^2*((21*Log[u]^6)/32 - (79*Log[u]^5*Log[v])/30 + 
        (29*Log[u]^4*Log[v]^2)/6 - (73*Log[u]^3*Log[v]^3)/12 + 
        (29*Log[u]^2*Log[v]^4)/6 - (79*Log[u]*Log[v]^5)/30 + 
        (21*Log[v]^6)/32 + (-Log[u]^5/8 + (19*Log[u]^4*Log[v])/24 + 
          (Log[u]^3*Log[v]^2)/6 + (Log[u]^2*Log[v]^3)/6 + 
          (19*Log[u]*Log[v]^4)/24 - Log[v]^5/8)*Log[w] + 
        ((25*Log[u]^4)/16 - (125*Log[u]^3*Log[v])/24 + (49*Log[u]^2*Log[v]^2)/
           8 - (125*Log[u]*Log[v]^3)/24 + (25*Log[v]^4)/16)*Log[w]^2 + 
        (-Log[u]^3/12 + (3*Log[u]^2*Log[v])/4 + (3*Log[u]*Log[v]^2)/4 - 
          Log[v]^3/12)*Log[w]^3 + ((41*Log[u]^2)/32 - (21*Log[u]*Log[v])/8 + 
          (41*Log[v]^2)/32)*Log[w]^4 + (3*Log[w]^6)/8 + 
        ((517*Log[u])/2 + (517*Log[v])/2 - 268*Log[w])*zeta[5]) + 
      zeta[2]^3*((-47837*Log[u]^6)/50400 - (47977*Log[u]^5*Log[v])/1050 + 
        (1847*Log[u]^4*Log[v]^2)/42 - (9817*Log[u]^3*Log[v]^3)/108 + 
        (1847*Log[u]^2*Log[v]^4)/42 - (47977*Log[u]*Log[v]^5)/1050 - 
        (47837*Log[v]^6)/50400 + ((-29159*Log[u]^5)/4200 - 
          (4769*Log[u]^4*Log[v])/140 - (671*Log[u]^3*Log[v]^2)/35 - 
          (671*Log[u]^2*Log[v]^3)/35 - (4769*Log[u]*Log[v]^4)/140 - 
          (29159*Log[v]^5)/4200)*Log[w] + ((269*Log[u]^4)/120 - 
          (14611*Log[u]^3*Log[v])/120 + (23489*Log[u]^2*Log[v]^2)/420 - 
          (14611*Log[u]*Log[v]^3)/120 + (269*Log[v]^4)/120)*Log[w]^2 + 
        ((-30253*Log[u]^3)/2520 - (12217*Log[u]^2*Log[v])/280 - 
          (12217*Log[u]*Log[v]^2)/280 - (30253*Log[v]^3)/2520)*Log[w]^3 + 
        ((17903*Log[u]^2)/1680 - (126781*Log[u]*Log[v])/1680 + 
          (17903*Log[v]^2)/1680)*Log[w]^4 + 
        ((-197*Log[u])/40 - (197*Log[v])/40)*Log[w]^5 + 
        (9371*Log[w]^6)/1260 + ((119333*Log[u]^3)/420 - (621*Log[u]^2*Log[v])/
           20 - (621*Log[u]*Log[v]^2)/20 + (119333*Log[v]^3)/420 + 
          ((544*Log[u]^2)/21 - (1269*Log[u]*Log[v])/35 + (544*Log[v]^2)/21)*
           Log[w] + ((8674*Log[u])/105 + (8674*Log[v])/105)*Log[w]^2 + 
          (47*Log[w]^3)/14)*zeta[3] + (44721*zeta[3]^2)/70 + 
        ((378341*Log[u])/210 + (378341*Log[v])/210 + (3912*Log[w])/35)*
         zeta[5]) + ((4561*Log[u]^5)/120 - (221*Log[u]^4*Log[v])/8 + 
        34*Log[u]^3*Log[v]^2 + 34*Log[u]^2*Log[v]^3 - (221*Log[u]*Log[v]^4)/
         8 + (4561*Log[v]^5)/120 + ((-217*Log[u]^4)/8 + (35*Log[u]^3*Log[v])/
           2 - (105*Log[u]^2*Log[v]^2)/2 + (35*Log[u]*Log[v]^3)/2 - 
          (217*Log[v]^4)/8)*Log[w] + ((93*Log[u]^3)/2 + (93*Log[v]^3)/2)*
         Log[w]^2 + ((-105*Log[u]^2)/4 - (105*Log[v]^2)/4)*Log[w]^3 + 
        ((75*Log[u])/8 + (75*Log[v])/8)*Log[w]^4)*zeta[7] + 
      zeta[5]*((5*Log[u]^7)/6 - (287*Log[u]^6*Log[v])/240 + 
        (223*Log[u]^5*Log[v]^2)/120 - (31*Log[u]^4*Log[v]^3)/72 - 
        (31*Log[u]^3*Log[v]^4)/72 + (223*Log[u]^2*Log[v]^5)/120 - 
        (287*Log[u]*Log[v]^6)/240 + (5*Log[v]^7)/6 + 
        ((-31*Log[u]^6)/48 + (101*Log[u]^5*Log[v])/120 - 
          (101*Log[u]^4*Log[v]^2)/48 + (5*Log[u]^3*Log[v]^3)/3 - 
          (101*Log[u]^2*Log[v]^4)/48 + (101*Log[u]*Log[v]^5)/120 - 
          (31*Log[v]^6)/48)*Log[w] + ((29*Log[u]^5)/16 - (65*Log[u]^4*Log[v])/
           48 + (13*Log[u]^3*Log[v]^2)/8 + (13*Log[u]^2*Log[v]^3)/8 - 
          (65*Log[u]*Log[v]^4)/48 + (29*Log[v]^5)/16)*Log[w]^2 + 
        ((-61*Log[u]^4)/48 + (5*Log[u]^3*Log[v])/6 - (5*Log[u]^2*Log[v]^2)/
           2 + (5*Log[u]*Log[v]^3)/6 - (61*Log[v]^4)/48)*Log[w]^3 + 
        ((55*Log[u]^3)/48 + (55*Log[v]^3)/48)*Log[w]^4 + 
        ((-5*Log[u]^2)/8 - (5*Log[v]^2)/8)*Log[w]^5 + (Log[u]/6 + Log[v]/6)*
         Log[w]^6 + 766*zeta[7]) + zeta[2]^2*((181*Log[u]^8)/3840 - 
        (557*Log[u]^7*Log[v])/400 + (8467*Log[u]^6*Log[v]^2)/3600 - 
        (553*Log[u]^5*Log[v]^3)/120 + (6553*Log[u]^4*Log[v]^4)/1440 - 
        (553*Log[u]^3*Log[v]^5)/120 + (8467*Log[u]^2*Log[v]^6)/3600 - 
        (557*Log[u]*Log[v]^7)/400 + (181*Log[v]^8)/3840 + 
        ((-17*Log[u]^7)/240 - (107*Log[u]^6*Log[v])/120 + 
          (13*Log[u]^5*Log[v]^2)/75 - (653*Log[u]^4*Log[v]^3)/720 - 
          (653*Log[u]^3*Log[v]^4)/720 + (13*Log[u]^2*Log[v]^5)/75 - 
          (107*Log[u]*Log[v]^6)/120 - (17*Log[v]^7)/240)*Log[w] + 
        ((149*Log[u]^6)/480 - (5911*Log[u]^5*Log[v])/1200 + 
          (2719*Log[u]^4*Log[v]^2)/480 - (737*Log[u]^3*Log[v]^3)/72 + 
          (2719*Log[u]^2*Log[v]^4)/480 - (5911*Log[u]*Log[v]^5)/1200 + 
          (149*Log[v]^6)/480)*Log[w]^2 + ((-11*Log[u]^5)/60 - 
          (31*Log[u]^4*Log[v])/16 - (13*Log[u]^3*Log[v]^2)/15 - 
          (13*Log[u]^2*Log[v]^3)/15 - (31*Log[u]*Log[v]^4)/16 - 
          (11*Log[v]^5)/60)*Log[w]^3 + ((329*Log[u]^4)/480 - 
          (2711*Log[u]^3*Log[v])/480 + (623*Log[u]^2*Log[v]^2)/160 - 
          (2711*Log[u]*Log[v]^3)/480 + (329*Log[v]^4)/480)*Log[w]^4 + 
        ((-37*Log[u]^3)/240 - (87*Log[u]^2*Log[v])/80 - (87*Log[u]*Log[v]^2)/
           80 - (37*Log[v]^3)/240)*Log[w]^5 + 
        ((201*Log[u]^2)/320 - (203*Log[u]*Log[v])/96 + (201*Log[v]^2)/320)*
         Log[w]^6 + (-Log[u]/24 - Log[v]/24)*Log[w]^7 + (791*Log[w]^8)/3840 + 
        ((2059*Log[u]^2)/10 - (3481*Log[u]*Log[v])/10 + (2059*Log[v]^2)/10 + 
          ((238*Log[u])/5 + (238*Log[v])/5)*Log[w] + (1691*Log[w]^2)/10)*
         zeta[3]^2 + ((26719*Log[u]^3)/60 + (11*Log[u]^2*Log[v])/4 + 
          (11*Log[u]*Log[v]^2)/4 + (26719*Log[v]^3)/60 + 
          ((-289*Log[u]^2)/5 - (81*Log[u]*Log[v])/5 - (289*Log[v]^2)/5)*
           Log[w] + ((622*Log[u])/5 + (622*Log[v])/5)*Log[w]^2 - 
          (161*Log[w]^3)/10)*zeta[5] + zeta[3]*((13531*Log[u]^5)/600 - 
          (73*Log[u]^4*Log[v])/4 + (469*Log[u]^3*Log[v]^2)/30 + 
          (469*Log[u]^2*Log[v]^3)/30 - (73*Log[u]*Log[v]^4)/4 + 
          (13531*Log[v]^5)/600 + ((-5*Log[u]^4)/8 + (97*Log[u]^3*Log[v])/30 - 
            (139*Log[u]^2*Log[v]^2)/10 + (97*Log[u]*Log[v]^3)/30 - 
            (5*Log[v]^4)/8)*Log[w] + ((969*Log[u]^3)/40 - 
            (293*Log[u]^2*Log[v])/40 - (293*Log[u]*Log[v]^2)/40 + 
            (969*Log[v]^3)/40)*Log[w]^2 + ((-29*Log[u]^2)/4 - 
            2*Log[u]*Log[v] - (29*Log[v]^2)/4)*Log[w]^3 + 
          ((51*Log[u])/16 + (51*Log[v])/16)*Log[w]^4 - (3*Log[w]^5)/20 + 
          (10163*zeta[5])/5) + ((31243*Log[u])/10 + (31243*Log[v])/10 - 
          189*Log[w])*zeta[7]) + ((9212*Log[u]^3)/9 + (9212*Log[v]^3)/9 + 
        (-588*Log[u]^2 - 588*Log[v]^2)*Log[w] + (392*Log[u] + 392*Log[v])*
         Log[w]^2)*zeta[9] + zeta[3]*((5*Log[u]^9)/384 - 
        (11*Log[u]^8*Log[v])/384 + (5*Log[u]^7*Log[v]^2)/96 - 
        (67*Log[u]^6*Log[v]^3)/1440 + (11*Log[u]^5*Log[v]^4)/480 + 
        (11*Log[u]^4*Log[v]^5)/480 - (67*Log[u]^3*Log[v]^6)/1440 + 
        (5*Log[u]^2*Log[v]^7)/96 - (11*Log[u]*Log[v]^8)/384 + 
        (5*Log[v]^9)/384 + (-Log[u]^8/96 + (Log[u]^7*Log[v])/48 - 
          (5*Log[u]^6*Log[v]^2)/96 + (23*Log[u]^5*Log[v]^3)/360 - 
          (Log[u]^4*Log[v]^4)/12 + (23*Log[u]^3*Log[v]^5)/360 - 
          (5*Log[u]^2*Log[v]^6)/96 + (Log[u]*Log[v]^7)/48 - Log[v]^8/96)*
         Log[w] + (Log[u]^7/24 - (Log[u]^6*Log[v])/16 + (3*Log[u]^5*Log[v]^2)/
           32 - (7*Log[u]^4*Log[v]^3)/288 - (7*Log[u]^3*Log[v]^4)/288 + 
          (3*Log[u]^2*Log[v]^5)/32 - (Log[u]*Log[v]^6)/16 + Log[v]^7/24)*
         Log[w]^2 + (-Log[u]^6/32 + (Log[u]^5*Log[v])/24 - 
          (5*Log[u]^4*Log[v]^2)/48 + (Log[u]^3*Log[v]^3)/12 - 
          (5*Log[u]^2*Log[v]^4)/48 + (Log[u]*Log[v]^5)/24 - Log[v]^6/32)*
         Log[w]^3 + ((3*Log[u]^5)/64 - (7*Log[u]^4*Log[v])/192 + 
          (Log[u]^3*Log[v]^2)/24 + (Log[u]^2*Log[v]^3)/24 - 
          (7*Log[u]*Log[v]^4)/192 + (3*Log[v]^5)/64)*Log[w]^4 + 
        (-Log[u]^4/32 + (Log[u]^3*Log[v])/48 - (Log[u]^2*Log[v]^2)/16 + 
          (Log[u]*Log[v]^3)/48 - Log[v]^4/32)*Log[w]^5 + 
        (Log[u]^3/48 + Log[v]^3/48)*Log[w]^6 + (-Log[u]^2/96 - Log[v]^2/96)*
         Log[w]^7 + (Log[u]/384 + Log[v]/384)*Log[w]^8 + 
        ((1181*Log[u]^4)/24 - (303*Log[u]^3*Log[v])/2 + 
          189*Log[u]^2*Log[v]^2 - (303*Log[u]*Log[v]^3)/2 + 
          (1181*Log[v]^4)/24 + ((-25*Log[u]^3)/6 + (59*Log[u]^2*Log[v])/2 + 
            (59*Log[u]*Log[v]^2)/2 - (25*Log[v]^3)/6)*Log[w] + 
          ((151*Log[u]^2)/2 - 151*Log[u]*Log[v] + (151*Log[v]^2)/2)*
           Log[w]^2 + ((-3*Log[u])/2 - (3*Log[v])/2)*Log[w]^3 + 
          (61*Log[w]^4)/2)*zeta[5] + (974*Log[u]^2 - 1914*Log[u]*Log[v] + 
          974*Log[v]^2 + (-48*Log[u] - 48*Log[v])*Log[w] + 660*Log[w]^2)*
         zeta[7] + 784*zeta[9]) + zeta[2]*((7*Log[u]^10)/3840 - 
        (31*Log[u]^9*Log[v])/1152 + (49*Log[u]^8*Log[v]^2)/768 - 
        (49*Log[u]^7*Log[v]^3)/360 + (641*Log[u]^6*Log[v]^4)/3456 - 
        (1571*Log[u]^5*Log[v]^5)/7200 + (641*Log[u]^4*Log[v]^6)/3456 - 
        (49*Log[u]^3*Log[v]^7)/360 + (49*Log[u]^2*Log[v]^8)/768 - 
        (31*Log[u]*Log[v]^9)/1152 + (7*Log[v]^10)/3840 + 
        (-(Log[u]^8*Log[v])/96 + (Log[u]^7*Log[v]^2)/96 - 
          (Log[u]^6*Log[v]^3)/48 + (Log[u]^5*Log[v]^4)/720 + 
          (Log[u]^4*Log[v]^5)/720 - (Log[u]^3*Log[v]^6)/48 + 
          (Log[u]^2*Log[v]^7)/96 - (Log[u]*Log[v]^8)/96)*Log[w] + 
        (Log[u]^8/96 - (11*Log[u]^7*Log[v])/96 + (41*Log[u]^6*Log[v]^2)/192 - 
          (581*Log[u]^5*Log[v]^3)/1440 + (59*Log[u]^4*Log[v]^4)/144 - 
          (581*Log[u]^3*Log[v]^5)/1440 + (41*Log[u]^2*Log[v]^6)/192 - 
          (11*Log[u]*Log[v]^7)/96 + Log[v]^8/96)*Log[w]^2 + 
        (-(Log[u]^6*Log[v])/32 + (Log[u]^5*Log[v]^2)/96 - 
          (Log[u]^4*Log[v]^3)/32 - (Log[u]^3*Log[v]^4)/32 + 
          (Log[u]^2*Log[v]^5)/96 - (Log[u]*Log[v]^6)/32)*Log[w]^3 + 
        ((3*Log[u]^6)/128 - (35*Log[u]^5*Log[v])/192 + (Log[u]^4*Log[v]^2)/
           4 - (19*Log[u]^3*Log[v]^3)/48 + (Log[u]^2*Log[v]^4)/4 - 
          (35*Log[u]*Log[v]^5)/192 + (3*Log[v]^6)/128)*Log[w]^4 + 
        (-(Log[u]^4*Log[v])/32 - (Log[u]^3*Log[v]^2)/96 - 
          (Log[u]^2*Log[v]^3)/96 - (Log[u]*Log[v]^4)/32)*Log[w]^5 + 
        ((5*Log[u]^4)/192 - (37*Log[u]^3*Log[v])/288 + (11*Log[u]^2*Log[v]^2)/
           96 - (37*Log[u]*Log[v]^3)/288 + (5*Log[v]^4)/192)*Log[w]^6 + 
        (-(Log[u]^2*Log[v])/96 - (Log[u]*Log[v]^2)/96)*Log[w]^7 + 
        ((11*Log[u]^2)/768 - (13*Log[u]*Log[v])/384 + (11*Log[v]^2)/768)*
         Log[w]^8 + Log[w]^10/320 + ((935*Log[u]^4)/48 - 
          (689*Log[u]^3*Log[v])/12 + (149*Log[u]^2*Log[v]^2)/2 - 
          (689*Log[u]*Log[v]^3)/12 + (935*Log[v]^4)/48 + 
          ((31*Log[u]^3)/12 + (37*Log[u]^2*Log[v])/4 + (37*Log[u]*Log[v]^2)/
             4 + (31*Log[v]^3)/12)*Log[w] + ((129*Log[u]^2)/4 - 
            (121*Log[u]*Log[v])/2 + (129*Log[v]^2)/4)*Log[w]^2 + 
          ((9*Log[u])/4 + (9*Log[v])/4)*Log[w]^3 + (55*Log[w]^4)/4)*
         zeta[3]^2 + ((379*Log[u])/6 + (379*Log[v])/6 - 46*Log[w])*
         zeta[3]^3 + ((482*Log[u]^5)/15 - (271*Log[u]^4*Log[v])/12 + 
          (80*Log[u]^3*Log[v]^2)/3 + (80*Log[u]^2*Log[v]^3)/3 - 
          (271*Log[u]*Log[v]^4)/12 + (482*Log[v]^5)/15 + 
          ((-139*Log[u]^4)/12 + (32*Log[u]^3*Log[v])/3 - 31*Log[u]^2*
             Log[v]^2 + (32*Log[u]*Log[v]^3)/3 - (139*Log[v]^4)/12)*Log[w] + 
          ((421*Log[u]^3)/12 - (17*Log[u]^2*Log[v])/4 - (17*Log[u]*Log[v]^2)/
             4 + (421*Log[v]^3)/12)*Log[w]^2 + ((-33*Log[u]^2)/2 - 
            (33*Log[v]^2)/2)*Log[w]^3 + ((45*Log[u])/8 + (45*Log[v])/8)*
           Log[w]^4 - Log[w]^5/2)*zeta[5] + 1579*zeta[5]^2 + 
        ((2159*Log[u]^3)/3 + 24*Log[u]^2*Log[v] + 24*Log[u]*Log[v]^2 + 
          (2159*Log[v]^3)/3 + ((-513*Log[u]^2)/2 - (513*Log[v]^2)/2)*Log[w] + 
          (234*Log[u] + 234*Log[v])*Log[w]^2 - 30*Log[w]^3)*zeta[7] + 
        zeta[3]*((5*Log[u]^7)/6 - (99*Log[u]^6*Log[v])/80 + 
          (203*Log[u]^5*Log[v]^2)/120 - (13*Log[u]^4*Log[v]^3)/24 - 
          (13*Log[u]^3*Log[v]^4)/24 + (203*Log[u]^2*Log[v]^5)/120 - 
          (99*Log[u]*Log[v]^6)/80 + (5*Log[v]^7)/6 + 
          ((-13*Log[u]^6)/48 + (61*Log[u]^5*Log[v])/120 - 
            (23*Log[u]^4*Log[v]^2)/16 + (4*Log[u]^3*Log[v]^3)/3 - 
            (23*Log[u]^2*Log[v]^4)/16 + (61*Log[u]*Log[v]^5)/120 - 
            (13*Log[v]^6)/48)*Log[w] + ((27*Log[u]^5)/16 - 
            (73*Log[u]^4*Log[v])/48 + (31*Log[u]^3*Log[v]^2)/24 + 
            (31*Log[u]^2*Log[v]^3)/24 - (73*Log[u]*Log[v]^4)/48 + 
            (27*Log[v]^5)/16)*Log[w]^2 + ((-37*Log[u]^4)/48 + 
            (2*Log[u]^3*Log[v])/3 - 2*Log[u]^2*Log[v]^2 + (2*Log[u]*Log[v]^3)/
             3 - (37*Log[v]^4)/48)*Log[w]^3 + ((47*Log[u]^3)/48 - 
            (Log[u]^2*Log[v])/4 - (Log[u]*Log[v]^2)/4 + (47*Log[v]^3)/48)*
           Log[w]^4 + (-Log[u]^2/2 - Log[v]^2/2)*Log[w]^5 + 
          (Log[u]/8 + Log[v]/8)*Log[w]^6 + (682*Log[u]^2 - 
            1246*Log[u]*Log[v] + 682*Log[v]^2 + (84*Log[u] + 84*Log[v])*
             Log[w] + 508*Log[w]^2)*zeta[5] + 3206*zeta[7]) + 
        ((17248*Log[u])/3 + (17248*Log[v])/3 - 784*Log[w])*zeta[9]) + 
      (8820*Log[u] + 8820*Log[v])*zeta[11]) + 
    g^14*(EO[7, 14] + EO[7, 13]*Log[w] + 
      (-Log[u]^12/92160 + (Log[u]^11*Log[v])/7680 - (Log[u]^10*Log[v]^2)/
         2560 + (13*Log[u]^9*Log[v]^3)/13824 - (29*Log[u]^8*Log[v]^4)/18432 + 
        (7*Log[u]^7*Log[v]^5)/3200 - (619*Log[u]^6*Log[v]^6)/259200 + 
        (7*Log[u]^5*Log[v]^7)/3200 - (29*Log[u]^4*Log[v]^8)/18432 + 
        (13*Log[u]^3*Log[v]^9)/13824 - (Log[u]^2*Log[v]^10)/2560 + 
        (Log[u]*Log[v]^11)/7680 - Log[v]^12/92160)*Log[w]^2 + 
      (-Log[u]^10/30720 + (Log[u]^9*Log[v])/3072 - (5*Log[u]^8*Log[v]^2)/
         6144 + (Log[u]^7*Log[v]^3)/576 - (11*Log[u]^6*Log[v]^4)/4608 + 
        (163*Log[u]^5*Log[v]^5)/57600 - (11*Log[u]^4*Log[v]^6)/4608 + 
        (Log[u]^3*Log[v]^7)/576 - (5*Log[u]^2*Log[v]^8)/6144 + 
        (Log[u]*Log[v]^9)/3072 - Log[v]^10/30720)*Log[w]^4 + 
      (-Log[u]^8/18432 + (Log[u]^7*Log[v])/2304 - (Log[u]^6*Log[v]^2)/1152 + 
        (11*Log[u]^5*Log[v]^3)/6912 - (23*Log[u]^4*Log[v]^4)/13824 + 
        (11*Log[u]^3*Log[v]^5)/6912 - (Log[u]^2*Log[v]^6)/1152 + 
        (Log[u]*Log[v]^7)/2304 - Log[v]^8/18432)*Log[w]^6 + 
      (-Log[u]^6/18432 + (Log[u]^5*Log[v])/3072 - (Log[u]^4*Log[v]^2)/2048 + 
        (5*Log[u]^3*Log[v]^3)/6912 - (Log[u]^2*Log[v]^4)/2048 + 
        (Log[u]*Log[v]^5)/3072 - Log[v]^6/18432)*Log[w]^8 + 
      (-Log[u]^4/30720 + (Log[u]^3*Log[v])/7680 - (Log[u]^2*Log[v]^2)/7680 + 
        (Log[u]*Log[v]^3)/7680 - Log[v]^4/30720)*Log[w]^10 + 
      (-Log[u]^2/92160 + (Log[u]*Log[v])/46080 - Log[v]^2/92160)*Log[w]^12 - 
      Log[w]^14/645120 + (((16249657*Log[u]^2)/12320 + 
          (154149467*Log[u]*Log[v])/13200 + (16249657*Log[v]^2)/12320)*
         Log[w]^2 + ((1210747*Log[u])/525 + (1210747*Log[v])/525)*Log[w]^3 - 
        (135498689*Log[w]^4)/184800)*zeta[2]^5 - 
      (186048404027*Log[w]^2*zeta[2]^6)/252252000 + 
      (((-859*Log[u]^3)/72 + (9*Log[u]^2*Log[v])/8 + (9*Log[u]*Log[v]^2)/8 - 
          (859*Log[v]^3)/72)*Log[w]^2 + ((37*Log[u]^2)/4 - 
          (7*Log[u]*Log[v])/2 + (37*Log[v]^2)/4)*Log[w]^3 + 
        ((-29*Log[u])/8 - (29*Log[v])/8)*Log[w]^4 + (9*Log[w]^5)/8)*
       zeta[3]^3 - (131*Log[w]^2*zeta[3]^4)/8 + 
      zeta[2]^4*(((3898513*Log[u]^4)/67200 + (49689887*Log[u]^3*Log[v])/
           50400 - (699359*Log[u]^2*Log[v]^2)/2400 + 
          (49689887*Log[u]*Log[v]^3)/50400 + (3898513*Log[v]^4)/67200)*
         Log[w]^2 + ((2036017*Log[u]^3)/12600 + (1876729*Log[u]^2*Log[v])/
           4200 + (1876729*Log[u]*Log[v]^2)/4200 + (2036017*Log[v]^3)/12600)*
         Log[w]^3 + ((-427167*Log[u]^2)/22400 + (7243839*Log[u]*Log[v])/
           11200 - (427167*Log[v]^2)/22400)*Log[w]^4 + 
        ((80527*Log[u])/1200 + (80527*Log[v])/1200)*Log[w]^5 - 
        (1925537*Log[w]^6)/40320 + 
        (((-264311*Log[u])/560 - (264311*Log[v])/560)*Log[w]^2 - 
          (8998*Log[w]^3)/525)*zeta[3]) + 
      (((-797*Log[u]^2)/2 + (1541*Log[u]*Log[v])/2 - (797*Log[v]^2)/2)*
         Log[w]^2 + (15*Log[u] + 15*Log[v])*Log[w]^3 - (313*Log[w]^4)/2)*
       zeta[5]^2 + zeta[2]^3*(((5987*Log[u]^6)/100800 + 
          (5589*Log[u]^5*Log[v])/140 - (25729*Log[u]^4*Log[v]^2)/672 + 
          (601703*Log[u]^3*Log[v]^3)/7560 - (25729*Log[u]^2*Log[v]^4)/672 + 
          (5589*Log[u]*Log[v]^5)/140 + (5987*Log[v]^6)/100800)*Log[w]^2 + 
        ((24923*Log[u]^5)/6300 + (6871*Log[u]^4*Log[v])/315 + 
          (14899*Log[u]^3*Log[v]^2)/1260 + (14899*Log[u]^2*Log[v]^3)/1260 + 
          (6871*Log[u]*Log[v]^4)/315 + (24923*Log[v]^5)/6300)*Log[w]^3 + 
        ((-2797*Log[u]^4)/1344 + (54863*Log[u]^3*Log[v])/1120 - 
          (4103*Log[u]^2*Log[v]^2)/168 + (54863*Log[u]*Log[v]^3)/1120 - 
          (2797*Log[v]^4)/1344)*Log[w]^4 + ((34271*Log[u]^3)/10080 + 
          (15199*Log[u]^2*Log[v])/1120 + (15199*Log[u]*Log[v]^2)/1120 + 
          (34271*Log[v]^3)/10080)*Log[w]^5 + ((-8651*Log[u]^2)/2520 + 
          (38723*Log[u]*Log[v])/2016 - (8651*Log[v]^2)/2520)*Log[w]^6 + 
        ((37*Log[u])/40 + (37*Log[v])/40)*Log[w]^7 - (12223*Log[w]^8)/8064 + 
        (((-136223*Log[u]^3)/840 + (22621*Log[u]^2*Log[v])/280 + 
            (22621*Log[u]*Log[v]^2)/280 - (136223*Log[v]^3)/840)*Log[w]^2 + 
          ((5177*Log[u]^2)/140 + (1607*Log[u]*Log[v])/42 + 
            (5177*Log[v]^2)/140)*Log[w]^3 + ((-3833*Log[u])/420 - 
            (3833*Log[v])/420)*Log[w]^4 + (969*Log[w]^5)/280)*zeta[3] - 
        (161261*Log[w]^2*zeta[3]^2)/140 + 
        (((-275873*Log[u])/420 - (275873*Log[v])/420)*Log[w]^2 + 
          (2717*Log[w]^3)/21)*zeta[5]) + zeta[3]^2*
       (((-77*Log[u]^6)/192 + (407*Log[u]^5*Log[v])/240 - 
          (287*Log[u]^4*Log[v]^2)/96 + (31*Log[u]^3*Log[v]^3)/8 - 
          (287*Log[u]^2*Log[v]^4)/96 + (407*Log[u]*Log[v]^5)/240 - 
          (77*Log[v]^6)/192)*Log[w]^2 + (Log[u]^5/16 - (19*Log[u]^4*Log[v])/
           48 - (Log[u]^3*Log[v]^2)/12 - (Log[u]^2*Log[v]^3)/12 - 
          (19*Log[u]*Log[v]^4)/48 + Log[v]^5/16)*Log[w]^3 + 
        ((-47*Log[u]^4)/96 + (161*Log[u]^3*Log[v])/96 - 
          (61*Log[u]^2*Log[v]^2)/32 + (161*Log[u]*Log[v]^3)/96 - 
          (47*Log[v]^4)/96)*Log[w]^4 + (Log[u]^3/48 - (3*Log[u]^2*Log[v])/
           16 - (3*Log[u]*Log[v]^2)/16 + Log[v]^3/48)*Log[w]^5 + 
        ((-53*Log[u]^2)/192 + (9*Log[u]*Log[v])/16 - (53*Log[v]^2)/192)*
         Log[w]^6 - Log[w]^8/16 + (((-1077*Log[u])/4 - (1077*Log[v])/4)*
           Log[w]^2 + 135*Log[w]^3)*zeta[5]) + 
      (((-4561*Log[u]^5)/240 + (221*Log[u]^4*Log[v])/16 - 
          17*Log[u]^3*Log[v]^2 - 17*Log[u]^2*Log[v]^3 + (221*Log[u]*Log[v]^4)/
           16 - (4561*Log[v]^5)/240)*Log[w]^2 + 
        ((217*Log[u]^4)/16 - (35*Log[u]^3*Log[v])/4 + (105*Log[u]^2*Log[v]^2)/
           4 - (35*Log[u]*Log[v]^3)/4 + (217*Log[v]^4)/16)*Log[w]^3 + 
        ((-93*Log[u]^3)/8 - (93*Log[v]^3)/8)*Log[w]^4 + 
        ((105*Log[u]^2)/16 + (105*Log[v]^2)/16)*Log[w]^5 + 
        ((-25*Log[u])/16 - (25*Log[v])/16)*Log[w]^6)*zeta[7] + 
      zeta[5]*(((-5*Log[u]^7)/12 + (287*Log[u]^6*Log[v])/480 - 
          (223*Log[u]^5*Log[v]^2)/240 + (31*Log[u]^4*Log[v]^3)/144 + 
          (31*Log[u]^3*Log[v]^4)/144 - (223*Log[u]^2*Log[v]^5)/240 + 
          (287*Log[u]*Log[v]^6)/480 - (5*Log[v]^7)/12)*Log[w]^2 + 
        ((31*Log[u]^6)/96 - (101*Log[u]^5*Log[v])/240 + 
          (101*Log[u]^4*Log[v]^2)/96 - (5*Log[u]^3*Log[v]^3)/6 + 
          (101*Log[u]^2*Log[v]^4)/96 - (101*Log[u]*Log[v]^5)/240 + 
          (31*Log[v]^6)/96)*Log[w]^3 + ((-29*Log[u]^5)/64 + 
          (65*Log[u]^4*Log[v])/192 - (13*Log[u]^3*Log[v]^2)/32 - 
          (13*Log[u]^2*Log[v]^3)/32 + (65*Log[u]*Log[v]^4)/192 - 
          (29*Log[v]^5)/64)*Log[w]^4 + ((61*Log[u]^4)/192 - 
          (5*Log[u]^3*Log[v])/24 + (5*Log[u]^2*Log[v]^2)/8 - 
          (5*Log[u]*Log[v]^3)/24 + (61*Log[v]^4)/192)*Log[w]^5 + 
        ((-55*Log[u]^3)/288 - (55*Log[v]^3)/288)*Log[w]^6 + 
        ((5*Log[u]^2)/48 + (5*Log[v]^2)/48)*Log[w]^7 + 
        (-Log[u]/48 - Log[v]/48)*Log[w]^8 - 6893*Log[w]^2*zeta[7]) + 
      zeta[2]^2*(((-349*Log[u]^8)/7680 + (151*Log[u]^7*Log[v])/150 - 
          (24599*Log[u]^6*Log[v]^2)/14400 + (161*Log[u]^5*Log[v]^3)/48 - 
          (1897*Log[u]^4*Log[v]^4)/576 + (161*Log[u]^3*Log[v]^5)/48 - 
          (24599*Log[u]^2*Log[v]^6)/14400 + (151*Log[u]*Log[v]^7)/150 - 
          (349*Log[v]^8)/7680)*Log[w]^2 + ((17*Log[u]^7)/480 + 
          (61*Log[u]^6*Log[v])/120 - (43*Log[u]^5*Log[v]^2)/400 + 
          (743*Log[u]^4*Log[v]^3)/1440 + (743*Log[u]^3*Log[v]^4)/1440 - 
          (43*Log[u]^2*Log[v]^5)/400 + (61*Log[u]*Log[v]^6)/120 + 
          (17*Log[v]^7)/480)*Log[w]^3 + ((-31*Log[u]^6)/240 + 
          (8221*Log[u]^5*Log[v])/4800 - (775*Log[u]^4*Log[v]^2)/384 + 
          (1031*Log[u]^3*Log[v]^3)/288 - (775*Log[u]^2*Log[v]^4)/384 + 
          (8221*Log[u]*Log[v]^5)/4800 - (31*Log[v]^6)/240)*Log[w]^4 + 
        ((11*Log[u]^5)/240 + (35*Log[u]^4*Log[v])/64 + (19*Log[u]^3*Log[v]^2)/
           80 + (19*Log[u]^2*Log[v]^3)/80 + (35*Log[u]*Log[v]^4)/64 + 
          (11*Log[v]^5)/240)*Log[w]^5 + ((-25*Log[u]^4)/144 + 
          (733*Log[u]^3*Log[v])/576 - (881*Log[u]^2*Log[v]^2)/960 + 
          (733*Log[u]*Log[v]^3)/576 - (25*Log[v]^4)/144)*Log[w]^6 + 
        ((37*Log[u]^3)/1440 + (97*Log[u]^2*Log[v])/480 + (97*Log[u]*Log[v]^2)/
           480 + (37*Log[v]^3)/1440)*Log[w]^7 + 
        ((-287*Log[u]^2)/2560 + (1343*Log[u]*Log[v])/3840 - 
          (287*Log[v]^2)/2560)*Log[w]^8 + (Log[u]/192 + Log[v]/192)*
         Log[w]^9 - (1079*Log[w]^10)/38400 + 
        (((-1186*Log[u]^2)/5 + (8071*Log[u]*Log[v])/20 - (1186*Log[v]^2)/5)*
           Log[w]^2 + (-27*Log[u] - 27*Log[v])*Log[w]^3 - (413*Log[w]^4)/4)*
         zeta[3]^2 + (((-27827*Log[u]^3)/120 + (497*Log[u]^2*Log[v])/8 + 
            (497*Log[u]*Log[v]^2)/8 - (27827*Log[v]^3)/120)*Log[w]^2 + 
          ((1783*Log[u]^2)/20 + (181*Log[u]*Log[v])/10 + (1783*Log[v]^2)/20)*
           Log[w]^3 + ((-747*Log[u])/40 - (747*Log[v])/40)*Log[w]^4 + 
          (301*Log[w]^5)/40)*zeta[5] + zeta[3]*
         (((-63*Log[u]^5)/5 + (629*Log[u]^4*Log[v])/48 - 
            (461*Log[u]^3*Log[v]^2)/60 - (461*Log[u]^2*Log[v]^3)/60 + 
            (629*Log[u]*Log[v]^4)/48 - (63*Log[v]^5)/5)*Log[w]^2 + 
          ((191*Log[u]^4)/60 - (181*Log[u]^3*Log[v])/60 + 
            (263*Log[u]^2*Log[v]^2)/20 - (181*Log[u]*Log[v]^3)/60 + 
            (191*Log[v]^4)/60)*Log[w]^3 + ((-2989*Log[u]^3)/480 + 
            (631*Log[u]^2*Log[v])/160 + (631*Log[u]*Log[v]^2)/160 - 
            (2989*Log[v]^3)/480)*Log[w]^4 + ((279*Log[u]^2)/80 + 
            (3*Log[u]*Log[v])/4 + (279*Log[v]^2)/80)*Log[w]^5 + 
          ((-163*Log[u])/480 - (163*Log[v])/480)*Log[w]^6 + Log[w]^7/15 - 
          (6855*Log[w]^2*zeta[5])/2) + (((-4799*Log[u])/4 - (4799*Log[v])/4)*
           Log[w]^2 + (519*Log[w]^3)/2)*zeta[7]) + 
      (((-4606*Log[u]^3)/9 - (4606*Log[v]^3)/9)*Log[w]^2 + 
        (294*Log[u]^2 + 294*Log[v]^2)*Log[w]^3 + (-98*Log[u] - 98*Log[v])*
         Log[w]^4)*zeta[9] + zeta[3]*
       (((-5*Log[u]^9)/768 + (11*Log[u]^8*Log[v])/768 - (5*Log[u]^7*Log[v]^2)/
           192 + (67*Log[u]^6*Log[v]^3)/2880 - (11*Log[u]^5*Log[v]^4)/960 - 
          (11*Log[u]^4*Log[v]^5)/960 + (67*Log[u]^3*Log[v]^6)/2880 - 
          (5*Log[u]^2*Log[v]^7)/192 + (11*Log[u]*Log[v]^8)/768 - 
          (5*Log[v]^9)/768)*Log[w]^2 + (Log[u]^8/192 - (Log[u]^7*Log[v])/96 + 
          (5*Log[u]^6*Log[v]^2)/192 - (23*Log[u]^5*Log[v]^3)/720 + 
          (Log[u]^4*Log[v]^4)/24 - (23*Log[u]^3*Log[v]^5)/720 + 
          (5*Log[u]^2*Log[v]^6)/192 - (Log[u]*Log[v]^7)/96 + Log[v]^8/192)*
         Log[w]^3 + (-Log[u]^7/96 + (Log[u]^6*Log[v])/64 - 
          (3*Log[u]^5*Log[v]^2)/128 + (7*Log[u]^4*Log[v]^3)/1152 + 
          (7*Log[u]^3*Log[v]^4)/1152 - (3*Log[u]^2*Log[v]^5)/128 + 
          (Log[u]*Log[v]^6)/64 - Log[v]^7/96)*Log[w]^4 + 
        (Log[u]^6/128 - (Log[u]^5*Log[v])/96 + (5*Log[u]^4*Log[v]^2)/192 - 
          (Log[u]^3*Log[v]^3)/48 + (5*Log[u]^2*Log[v]^4)/192 - 
          (Log[u]*Log[v]^5)/96 + Log[v]^6/128)*Log[w]^5 + 
        (-Log[u]^5/128 + (7*Log[u]^4*Log[v])/1152 - (Log[u]^3*Log[v]^2)/144 - 
          (Log[u]^2*Log[v]^3)/144 + (7*Log[u]*Log[v]^4)/1152 - Log[v]^5/128)*
         Log[w]^6 + (Log[u]^4/192 - (Log[u]^3*Log[v])/288 + 
          (Log[u]^2*Log[v]^2)/96 - (Log[u]*Log[v]^3)/288 + Log[v]^4/192)*
         Log[w]^7 + (-Log[u]^3/384 - Log[v]^3/384)*Log[w]^8 + 
        (Log[u]^2/768 + Log[v]^2/768)*Log[w]^9 + (-Log[u]/3840 - Log[v]/3840)*
         Log[w]^10 + (((-1573*Log[u]^4)/48 + (1271*Log[u]^3*Log[v])/12 - 
            125*Log[u]^2*Log[v]^2 + (1271*Log[u]*Log[v]^3)/12 - 
            (1573*Log[v]^4)/48)*Log[w]^2 + ((25*Log[u]^3)/12 - 
            (59*Log[u]^2*Log[v])/4 - (59*Log[u]*Log[v]^2)/4 + 
            (25*Log[v]^3)/12)*Log[w]^3 + ((-53*Log[u]^2)/2 + 
            (211*Log[u]*Log[v])/4 - (53*Log[v]^2)/2)*Log[w]^4 + 
          ((3*Log[u])/8 + (3*Log[v])/8)*Log[w]^5 - (91*Log[w]^6)/12)*
         zeta[5] + (((-1625*Log[u]^2)/2 + 1587*Log[u]*Log[v] - 
            (1625*Log[v]^2)/2)*Log[w]^2 + (24*Log[u] + 24*Log[v])*Log[w]^3 - 
          (645*Log[w]^4)/2)*zeta[7] - 7448*Log[w]^2*zeta[9]) + 
      zeta[2]*(((-3*Log[u]^10)/2560 + (37*Log[u]^9*Log[v])/2304 - 
          (59*Log[u]^8*Log[v]^2)/1536 + (59*Log[u]^7*Log[v]^3)/720 - 
          (773*Log[u]^6*Log[v]^4)/6912 + (1897*Log[u]^5*Log[v]^5)/14400 - 
          (773*Log[u]^4*Log[v]^6)/6912 + (59*Log[u]^3*Log[v]^7)/720 - 
          (59*Log[u]^2*Log[v]^8)/1536 + (37*Log[u]*Log[v]^9)/2304 - 
          (3*Log[v]^10)/2560)*Log[w]^2 + ((Log[u]^8*Log[v])/192 - 
          (Log[u]^7*Log[v]^2)/192 + (Log[u]^6*Log[v]^3)/96 - 
          (Log[u]^5*Log[v]^4)/1440 - (Log[u]^4*Log[v]^5)/1440 + 
          (Log[u]^3*Log[v]^6)/96 - (Log[u]^2*Log[v]^7)/192 + 
          (Log[u]*Log[v]^8)/192)*Log[w]^3 + ((-5*Log[u]^8)/1536 + 
          (13*Log[u]^7*Log[v])/384 - (49*Log[u]^6*Log[v]^2)/768 + 
          (691*Log[u]^5*Log[v]^3)/5760 - (47*Log[u]^4*Log[v]^4)/384 + 
          (691*Log[u]^3*Log[v]^5)/5760 - (49*Log[u]^2*Log[v]^6)/768 + 
          (13*Log[u]*Log[v]^7)/384 - (5*Log[v]^8)/1536)*Log[w]^4 + 
        ((Log[u]^6*Log[v])/128 - (Log[u]^5*Log[v]^2)/384 + 
          (Log[u]^4*Log[v]^3)/128 + (Log[u]^3*Log[v]^4)/128 - 
          (Log[u]^2*Log[v]^5)/384 + (Log[u]*Log[v]^6)/128)*Log[w]^5 + 
        ((-11*Log[u]^6)/2304 + (41*Log[u]^5*Log[v])/1152 - 
          (19*Log[u]^4*Log[v]^2)/384 + (67*Log[u]^3*Log[v]^3)/864 - 
          (19*Log[u]^2*Log[v]^4)/384 + (41*Log[u]*Log[v]^5)/1152 - 
          (11*Log[v]^6)/2304)*Log[w]^6 + ((Log[u]^4*Log[v])/192 + 
          (Log[u]^3*Log[v]^2)/576 + (Log[u]^2*Log[v]^3)/576 + 
          (Log[u]*Log[v]^4)/192)*Log[w]^7 + 
        (-Log[u]^4/256 + (43*Log[u]^3*Log[v])/2304 - (13*Log[u]^2*Log[v]^2)/
           768 + (43*Log[u]*Log[v]^3)/2304 - Log[v]^4/256)*Log[w]^8 + 
        ((Log[u]^2*Log[v])/768 + (Log[u]*Log[v]^2)/768)*Log[w]^9 + 
        ((-13*Log[u]^2)/7680 + (Log[u]*Log[v])/256 - (13*Log[v]^2)/7680)*
         Log[w]^10 - (7*Log[w]^12)/23040 + 
        (((-501*Log[u]^4)/32 + (385*Log[u]^3*Log[v])/8 - 
            (245*Log[u]^2*Log[v]^2)/4 + (385*Log[u]*Log[v]^3)/8 - 
            (501*Log[v]^4)/32)*Log[w]^2 + ((-29*Log[u]^3)/24 - 
            (37*Log[u]^2*Log[v])/8 - (37*Log[u]*Log[v]^2)/8 - 
            (29*Log[v]^3)/24)*Log[w]^3 + ((-217*Log[u]^2)/16 + 
            (205*Log[u]*Log[v])/8 - (217*Log[v]^2)/16)*Log[w]^4 + 
          ((-9*Log[u])/16 - (9*Log[v])/16)*Log[w]^5 - (97*Log[w]^6)/24)*
         zeta[3]^2 + (((-787*Log[u])/12 - (787*Log[v])/12)*Log[w]^2 + 
          27*Log[w]^3)*zeta[3]^3 + 
        (((-1999*Log[u]^5)/120 + 14*Log[u]^4*Log[v] - (79*Log[u]^3*Log[v]^2)/
             6 - (79*Log[u]^2*Log[v]^3)/6 + 14*Log[u]*Log[v]^4 - 
            (1999*Log[v]^5)/120)*Log[w]^2 + ((25*Log[u]^4)/3 - 
            7*Log[u]^3*Log[v] + (41*Log[u]^2*Log[v]^2)/2 - 
            7*Log[u]*Log[v]^3 + (25*Log[v]^4)/3)*Log[w]^3 + 
          ((-143*Log[u]^3)/16 + (37*Log[u]^2*Log[v])/16 + 
            (37*Log[u]*Log[v]^2)/16 - (143*Log[v]^3)/16)*Log[w]^4 + 
          ((43*Log[u]^2)/8 + (43*Log[v]^2)/8)*Log[w]^5 + 
          ((-41*Log[u])/48 - (41*Log[v])/48)*Log[w]^6 + Log[w]^7/12)*
         zeta[5] - (5223*Log[w]^2*zeta[5]^2)/2 + 
        (((-2171*Log[u]^3)/6 + (81*Log[u]^2*Log[v])/2 + (81*Log[u]*Log[v]^2)/
             2 - (2171*Log[v]^3)/6)*Log[w]^2 + ((723*Log[u]^2)/4 + 
            (723*Log[v]^2)/4)*Log[w]^3 + (-51*Log[u] - 51*Log[v])*Log[w]^4 + 
          (15*Log[w]^5)/2)*zeta[7] + zeta[3]*
         (((-7*Log[u]^7)/16 + (337*Log[u]^6*Log[v])/480 - 
            (73*Log[u]^5*Log[v]^2)/80 + (Log[u]^4*Log[v]^3)/3 + 
            (Log[u]^3*Log[v]^4)/3 - (73*Log[u]^2*Log[v]^5)/80 + 
            (337*Log[u]*Log[v]^6)/480 - (7*Log[v]^7)/16)*Log[w]^2 + 
          ((19*Log[u]^6)/96 - (27*Log[u]^5*Log[v])/80 + 
            (89*Log[u]^4*Log[v]^2)/96 - (5*Log[u]^3*Log[v]^3)/6 + 
            (89*Log[u]^2*Log[v]^4)/96 - (27*Log[u]*Log[v]^5)/80 + 
            (19*Log[v]^6)/96)*Log[w]^3 + ((-85*Log[u]^5)/192 + 
            (29*Log[u]^4*Log[v])/64 - (31*Log[u]^3*Log[v]^2)/96 - 
            (31*Log[u]^2*Log[v]^3)/96 + (29*Log[u]*Log[v]^4)/64 - 
            (85*Log[v]^5)/192)*Log[w]^4 + ((49*Log[u]^4)/192 - 
            (5*Log[u]^3*Log[v])/24 + (5*Log[u]^2*Log[v]^2)/8 - 
            (5*Log[u]*Log[v]^3)/24 + (49*Log[v]^4)/192)*Log[w]^5 + 
          ((-49*Log[u]^3)/288 + (Log[u]^2*Log[v])/16 + (Log[u]*Log[v]^2)/16 - 
            (49*Log[v]^3)/288)*Log[w]^6 + ((5*Log[u]^2)/48 + (5*Log[v]^2)/48)*
           Log[w]^7 + (-Log[u]/64 - Log[v]/64)*Log[w]^8 + 
          ((-669*Log[u]^2 + 1227*Log[u]*Log[v] - 669*Log[v]^2)*Log[w]^2 + 
            (-40*Log[u] - 40*Log[v])*Log[w]^3 - 277*Log[w]^4)*zeta[5] - 
          5389*Log[w]^2*zeta[7]) + (((-7448*Log[u])/3 - (7448*Log[v])/3)*
           Log[w]^2 + 392*Log[w]^3)*zeta[9]) + (-4410*Log[u] - 4410*Log[v])*
       Log[w]^2*zeta[11]) + g^16*(EO[8, 16] + EO[8, 15]*Log[w] + 
      EO[8, 14]*Log[w]^2 + EO[8, 13]*Log[w]^3 + 
      (Log[u]^12/368640 - (Log[u]^11*Log[v])/30720 + (Log[u]^10*Log[v]^2)/
         10240 - (13*Log[u]^9*Log[v]^3)/55296 + (29*Log[u]^8*Log[v]^4)/
         73728 - (7*Log[u]^7*Log[v]^5)/12800 + (619*Log[u]^6*Log[v]^6)/
         1036800 - (7*Log[u]^5*Log[v]^7)/12800 + (29*Log[u]^4*Log[v]^8)/
         73728 - (13*Log[u]^3*Log[v]^9)/55296 + (Log[u]^2*Log[v]^10)/10240 - 
        (Log[u]*Log[v]^11)/30720 + Log[v]^12/368640)*Log[w]^4 + 
      (Log[u]^10/184320 - (Log[u]^9*Log[v])/18432 + (5*Log[u]^8*Log[v]^2)/
         36864 - (Log[u]^7*Log[v]^3)/3456 + (11*Log[u]^6*Log[v]^4)/27648 - 
        (163*Log[u]^5*Log[v]^5)/345600 + (11*Log[u]^4*Log[v]^6)/27648 - 
        (Log[u]^3*Log[v]^7)/3456 + (5*Log[u]^2*Log[v]^8)/36864 - 
        (Log[u]*Log[v]^9)/18432 + Log[v]^10/184320)*Log[w]^6 + 
      (Log[u]^8/147456 - (Log[u]^7*Log[v])/18432 + (Log[u]^6*Log[v]^2)/9216 - 
        (11*Log[u]^5*Log[v]^3)/55296 + (23*Log[u]^4*Log[v]^4)/110592 - 
        (11*Log[u]^3*Log[v]^5)/55296 + (Log[u]^2*Log[v]^6)/9216 - 
        (Log[u]*Log[v]^7)/18432 + Log[v]^8/147456)*Log[w]^8 + 
      (Log[u]^6/184320 - (Log[u]^5*Log[v])/30720 + (Log[u]^4*Log[v]^2)/
         20480 - (Log[u]^3*Log[v]^3)/13824 + (Log[u]^2*Log[v]^4)/20480 - 
        (Log[u]*Log[v]^5)/30720 + Log[v]^6/184320)*Log[w]^10 + 
      (Log[u]^4/368640 - (Log[u]^3*Log[v])/92160 + (Log[u]^2*Log[v]^2)/
         92160 - (Log[u]*Log[v]^3)/92160 + Log[v]^4/368640)*Log[w]^12 + 
      (Log[u]^2/1290240 - (Log[u]*Log[v])/645120 + Log[v]^2/1290240)*
       Log[w]^14 + Log[w]^16/10321920 + 
      (((-1217703*Log[u]^2)/3200 - (2094987571*Log[u]*Log[v])/369600 - 
          (1217703*Log[v]^2)/3200)*Log[w]^4 + 
        ((-829379*Log[u])/1050 - (829379*Log[v])/1050)*Log[w]^5 + 
        (27129583*Log[w]^6)/92400)*zeta[2]^5 + 
      (679914594679*Log[w]^4*zeta[2]^6)/201801600 + 
      (((1189*Log[u]^3)/288 + (Log[u]^2*Log[v])/32 + (Log[u]*Log[v]^2)/32 + 
          (1189*Log[v]^3)/288)*Log[w]^4 + ((-21*Log[u]^2)/8 + 
          (7*Log[u]*Log[v])/8 - (21*Log[v]^2)/8)*Log[w]^5 + 
        ((13*Log[u])/16 + (13*Log[v])/16)*Log[w]^6 - (3*Log[w]^7)/16)*
       zeta[3]^3 + (403*Log[w]^4*zeta[3]^4)/32 + 
      zeta[2]^4*(((-161545*Log[u]^4)/10752 - (86691863*Log[u]^3*Log[v])/
           201600 + (9378217*Log[u]^2*Log[v]^2)/67200 - 
          (86691863*Log[u]*Log[v]^3)/201600 - (161545*Log[v]^4)/10752)*
         Log[w]^4 + ((-2537263*Log[u]^3)/50400 - (168051*Log[u]^2*Log[v])/
           1120 - (168051*Log[u]*Log[v]^2)/1120 - (2537263*Log[v]^3)/50400)*
         Log[w]^5 + ((105793*Log[u]^2)/8960 - (35427613*Log[u]*Log[v])/
           201600 + (105793*Log[v]^2)/8960)*Log[w]^6 + 
        ((-33109*Log[u])/2400 - (33109*Log[v])/2400)*Log[w]^7 + 
        (17230133*Log[w]^8)/1612800 + 
        (((-813671*Log[u])/33600 - (813671*Log[v])/33600)*Log[w]^4 - 
          (242*Log[w]^5)/5)*zeta[3]) + 
      ((139*Log[u]^2 - (2151*Log[u]*Log[v])/8 + 139*Log[v]^2)*Log[w]^4 + 
        ((-15*Log[u])/4 - (15*Log[v])/4)*Log[w]^5 + (931*Log[w]^6)/24)*
       zeta[5]^2 + zeta[2]^3*(((81223*Log[u]^6)/403200 - 
          (3089*Log[u]^5*Log[v])/200 + (33723*Log[u]^4*Log[v]^2)/2240 - 
          (935411*Log[u]^3*Log[v]^3)/30240 + (33723*Log[u]^2*Log[v]^4)/2240 - 
          (3089*Log[u]*Log[v]^5)/200 + (81223*Log[v]^6)/403200)*Log[w]^4 + 
        ((-111907*Log[u]^5)/100800 - (13655*Log[u]^4*Log[v])/2016 - 
          (1793*Log[u]^3*Log[v]^2)/504 - (1793*Log[u]^2*Log[v]^3)/504 - 
          (13655*Log[u]*Log[v]^4)/2016 - (111907*Log[v]^5)/100800)*Log[w]^5 + 
        ((493*Log[u]^4)/672 - (243253*Log[u]^3*Log[v])/20160 + 
          (64787*Log[u]^2*Log[v]^2)/10080 - (243253*Log[u]*Log[v]^3)/20160 + 
          (493*Log[v]^4)/672)*Log[w]^6 + ((-12763*Log[u]^3)/20160 - 
          (18461*Log[u]^2*Log[v])/6720 - (18461*Log[u]*Log[v]^2)/6720 - 
          (12763*Log[v]^3)/20160)*Log[w]^7 + ((57521*Log[u]^2)/80640 - 
          (39583*Log[u]*Log[v])/11520 + (57521*Log[v]^2)/80640)*Log[w]^8 + 
        ((-247*Log[u])/1920 - (247*Log[v])/1920)*Log[w]^9 + 
        (45901*Log[w]^10)/201600 + 
        (((43963*Log[u]^3)/1120 - (51703*Log[u]^2*Log[v])/1120 - 
            (51703*Log[u]*Log[v]^2)/1120 + (43963*Log[v]^3)/1120)*Log[w]^4 + 
          ((-22199*Log[u]^2)/840 - (13103*Log[u]*Log[v])/840 - 
            (22199*Log[v]^2)/840)*Log[w]^5 + ((-103*Log[u])/45 - 
            (103*Log[v])/45)*Log[w]^6 - (2453*Log[w]^7)/1680)*zeta[3] + 
        (1276843*Log[w]^4*zeta[3]^2)/1680 + 
        (((-10891*Log[u])/1680 - (10891*Log[v])/1680)*Log[w]^4 - 
          (19039*Log[w]^5)/210)*zeta[5]) + 
      zeta[3]^2*(((91*Log[u]^6)/768 - (83*Log[u]^5*Log[v])/160 + 
          (57*Log[u]^4*Log[v]^2)/64 - (113*Log[u]^3*Log[v]^3)/96 + 
          (57*Log[u]^2*Log[v]^4)/64 - (83*Log[u]*Log[v]^5)/160 + 
          (91*Log[v]^6)/768)*Log[w]^4 + (-Log[u]^5/64 + (19*Log[u]^4*Log[v])/
           192 + (Log[u]^3*Log[v]^2)/48 + (Log[u]^2*Log[v]^3)/48 + 
          (19*Log[u]*Log[v]^4)/192 - Log[v]^5/64)*Log[w]^5 + 
        ((113*Log[u]^4)/1152 - (197*Log[u]^3*Log[v])/576 + 
          (73*Log[u]^2*Log[v]^2)/192 - (197*Log[u]*Log[v]^3)/576 + 
          (113*Log[v]^4)/1152)*Log[w]^6 + 
        (-Log[u]^3/288 + (Log[u]^2*Log[v])/32 + (Log[u]*Log[v]^2)/32 - 
          Log[v]^3/288)*Log[w]^7 + ((65*Log[u]^2)/1536 - (11*Log[u]*Log[v])/
           128 + (65*Log[v]^2)/1536)*Log[w]^8 + Log[w]^10/128 + 
        (((1637*Log[u])/16 + (1637*Log[v])/16)*Log[w]^4 - 34*Log[w]^5)*
         zeta[5]) + (((4561*Log[u]^5)/960 - (221*Log[u]^4*Log[v])/64 + 
          (17*Log[u]^3*Log[v]^2)/4 + (17*Log[u]^2*Log[v]^3)/4 - 
          (221*Log[u]*Log[v]^4)/64 + (4561*Log[v]^5)/960)*Log[w]^4 + 
        ((-217*Log[u]^4)/64 + (35*Log[u]^3*Log[v])/16 - 
          (105*Log[u]^2*Log[v]^2)/16 + (35*Log[u]*Log[v]^3)/16 - 
          (217*Log[v]^4)/64)*Log[w]^5 + ((31*Log[u]^3)/16 + (31*Log[v]^3)/16)*
         Log[w]^6 + ((-35*Log[u]^2)/32 - (35*Log[v]^2)/32)*Log[w]^7 + 
        ((25*Log[u])/128 + (25*Log[v])/128)*Log[w]^8)*zeta[7] + 
      zeta[5]*(((5*Log[u]^7)/48 - (287*Log[u]^6*Log[v])/1920 + 
          (223*Log[u]^5*Log[v]^2)/960 - (31*Log[u]^4*Log[v]^3)/576 - 
          (31*Log[u]^3*Log[v]^4)/576 + (223*Log[u]^2*Log[v]^5)/960 - 
          (287*Log[u]*Log[v]^6)/1920 + (5*Log[v]^7)/48)*Log[w]^4 + 
        ((-31*Log[u]^6)/384 + (101*Log[u]^5*Log[v])/960 - 
          (101*Log[u]^4*Log[v]^2)/384 + (5*Log[u]^3*Log[v]^3)/24 - 
          (101*Log[u]^2*Log[v]^4)/384 + (101*Log[u]*Log[v]^5)/960 - 
          (31*Log[v]^6)/384)*Log[w]^5 + ((29*Log[u]^5)/384 - 
          (65*Log[u]^4*Log[v])/1152 + (13*Log[u]^3*Log[v]^2)/192 + 
          (13*Log[u]^2*Log[v]^3)/192 - (65*Log[u]*Log[v]^4)/1152 + 
          (29*Log[v]^5)/384)*Log[w]^6 + ((-61*Log[u]^4)/1152 + 
          (5*Log[u]^3*Log[v])/144 - (5*Log[u]^2*Log[v]^2)/48 + 
          (5*Log[u]*Log[v]^3)/144 - (61*Log[v]^4)/1152)*Log[w]^7 + 
        ((55*Log[u]^3)/2304 + (55*Log[v]^3)/2304)*Log[w]^8 + 
        ((-5*Log[u]^2)/384 - (5*Log[v]^2)/384)*Log[w]^9 + 
        (Log[u]/480 + Log[v]/480)*Log[w]^10 + (13403*Log[w]^4*zeta[7])/4) + 
      zeta[2]^2*(((557*Log[u]^8)/30720 - (1087*Log[u]^7*Log[v])/3200 + 
          (4183*Log[u]^6*Log[v]^2)/7200 - (3281*Log[u]^5*Log[v]^3)/2880 + 
          (12877*Log[u]^4*Log[v]^4)/11520 - (3281*Log[u]^3*Log[v]^5)/2880 + 
          (4183*Log[u]^2*Log[v]^6)/7200 - (1087*Log[u]*Log[v]^7)/3200 + 
          (557*Log[v]^8)/30720)*Log[w]^4 + ((-17*Log[u]^7)/1920 - 
          (137*Log[u]^6*Log[v])/960 + (77*Log[u]^5*Log[v]^2)/2400 - 
          (833*Log[u]^4*Log[v]^3)/5760 - (833*Log[u]^3*Log[v]^4)/5760 + 
          (77*Log[u]^2*Log[v]^5)/2400 - (137*Log[u]*Log[v]^6)/960 - 
          (17*Log[v]^7)/1920)*Log[w]^5 + ((367*Log[u]^6)/11520 - 
          (10831*Log[u]^5*Log[v])/28800 + (579*Log[u]^4*Log[v]^2)/1280 - 
          (455*Log[u]^3*Log[v]^3)/576 + (579*Log[u]^2*Log[v]^4)/1280 - 
          (10831*Log[u]*Log[v]^5)/28800 + (367*Log[v]^6)/11520)*Log[w]^6 + 
        ((-11*Log[u]^5)/1440 - (13*Log[u]^4*Log[v])/128 - 
          (31*Log[u]^3*Log[v]^2)/720 - (31*Log[u]^2*Log[v]^3)/720 - 
          (13*Log[u]*Log[v]^4)/128 - (11*Log[v]^5)/1440)*Log[w]^7 + 
        ((701*Log[u]^4)/23040 - (4739*Log[u]^3*Log[v])/23040 + 
          (393*Log[u]^2*Log[v]^2)/2560 - (4739*Log[u]*Log[v]^3)/23040 + 
          (701*Log[v]^4)/23040)*Log[w]^8 + ((-37*Log[u]^3)/11520 - 
          (107*Log[u]^2*Log[v])/3840 - (107*Log[u]*Log[v]^2)/3840 - 
          (37*Log[v]^3)/11520)*Log[w]^9 + ((1159*Log[u]^2)/76800 - 
          (1711*Log[u]*Log[v])/38400 + (1159*Log[v]^2)/76800)*Log[w]^10 + 
        (-Log[u]/1920 - Log[v]/1920)*Log[w]^11 + (469*Log[w]^12)/153600 + 
        (((9017*Log[u]^2)/80 - (15997*Log[u]*Log[v])/80 + (9017*Log[v]^2)/80)*
           Log[w]^4 + ((151*Log[u])/20 + (151*Log[v])/20)*Log[w]^5 + 
          (8177*Log[w]^6)/240)*zeta[3]^2 + 
        (((5411*Log[u]^3)/96 - (1165*Log[u]^2*Log[v])/32 - 
            (1165*Log[u]*Log[v]^2)/32 + (5411*Log[v]^3)/96)*Log[w]^4 + 
          ((-797*Log[u]^2)/20 - (281*Log[u]*Log[v])/40 - (797*Log[v]^2)/20)*
           Log[w]^5 + (Log[u]/24 + Log[v]/24)*Log[w]^6 - (147*Log[w]^7)/80)*
         zeta[5] + zeta[3]*(((5403*Log[u]^5)/1600 - (53*Log[u]^4*Log[v])/12 + 
            (413*Log[u]^3*Log[v]^2)/240 + (413*Log[u]^2*Log[v]^3)/240 - 
            (53*Log[u]*Log[v]^4)/12 + (5403*Log[v]^5)/1600)*Log[w]^4 + 
          ((-1573*Log[u]^4)/960 + (19*Log[u]^3*Log[v])/16 - 
            (407*Log[u]^2*Log[v]^2)/80 + (19*Log[u]*Log[v]^3)/16 - 
            (1573*Log[v]^4)/960)*Log[w]^5 + ((2911*Log[u]^3)/2880 - 
            (1049*Log[u]^2*Log[v])/960 - (1049*Log[u]*Log[v]^2)/960 + 
            (2911*Log[v]^3)/2880)*Log[w]^6 + ((-433*Log[u]^2)/480 - 
            (Log[u]*Log[v])/6 - (433*Log[v]^2)/480)*Log[w]^7 + 
          ((31*Log[u])/3840 + (31*Log[v])/3840)*Log[w]^8 - 
          (13*Log[w]^9)/960 + (83307*Log[w]^4*zeta[5])/40) + 
        (((11347*Log[u])/80 + (11347*Log[v])/80)*Log[w]^4 - (849*Log[w]^5)/8)*
         zeta[7]) + (((2303*Log[u]^3)/18 + (2303*Log[v]^3)/18)*Log[w]^4 + 
        ((-147*Log[u]^2)/2 - (147*Log[v]^2)/2)*Log[w]^5 + 
        ((49*Log[u])/3 + (49*Log[v])/3)*Log[w]^6)*zeta[9] + 
      zeta[3]*(((5*Log[u]^9)/3072 - (11*Log[u]^8*Log[v])/3072 + 
          (5*Log[u]^7*Log[v]^2)/768 - (67*Log[u]^6*Log[v]^3)/11520 + 
          (11*Log[u]^5*Log[v]^4)/3840 + (11*Log[u]^4*Log[v]^5)/3840 - 
          (67*Log[u]^3*Log[v]^6)/11520 + (5*Log[u]^2*Log[v]^7)/768 - 
          (11*Log[u]*Log[v]^8)/3072 + (5*Log[v]^9)/3072)*Log[w]^4 + 
        (-Log[u]^8/768 + (Log[u]^7*Log[v])/384 - (5*Log[u]^6*Log[v]^2)/768 + 
          (23*Log[u]^5*Log[v]^3)/2880 - (Log[u]^4*Log[v]^4)/96 + 
          (23*Log[u]^3*Log[v]^5)/2880 - (5*Log[u]^2*Log[v]^6)/768 + 
          (Log[u]*Log[v]^7)/384 - Log[v]^8/768)*Log[w]^5 + 
        (Log[u]^7/576 - (Log[u]^6*Log[v])/384 + (Log[u]^5*Log[v]^2)/256 - 
          (7*Log[u]^4*Log[v]^3)/6912 - (7*Log[u]^3*Log[v]^4)/6912 + 
          (Log[u]^2*Log[v]^5)/256 - (Log[u]*Log[v]^6)/384 + Log[v]^7/576)*
         Log[w]^6 + (-Log[u]^6/768 + (Log[u]^5*Log[v])/576 - 
          (5*Log[u]^4*Log[v]^2)/1152 + (Log[u]^3*Log[v]^3)/288 - 
          (5*Log[u]^2*Log[v]^4)/1152 + (Log[u]*Log[v]^5)/576 - Log[v]^6/768)*
         Log[w]^7 + (Log[u]^5/1024 - (7*Log[u]^4*Log[v])/9216 + 
          (Log[u]^3*Log[v]^2)/1152 + (Log[u]^2*Log[v]^3)/1152 - 
          (7*Log[u]*Log[v]^4)/9216 + Log[v]^5/1024)*Log[w]^8 + 
        (-Log[u]^4/1536 + (Log[u]^3*Log[v])/2304 - (Log[u]^2*Log[v]^2)/768 + 
          (Log[u]*Log[v]^3)/2304 - Log[v]^4/1536)*Log[w]^9 + 
        (Log[u]^3/3840 + Log[v]^3/3840)*Log[w]^10 + 
        (-Log[u]^2/7680 - Log[v]^2/7680)*Log[w]^11 + 
        (Log[u]/46080 + Log[v]/46080)*Log[w]^12 + 
        (((655*Log[u]^4)/64 - (1633*Log[u]^3*Log[v])/48 + 
            (311*Log[u]^2*Log[v]^2)/8 - (1633*Log[u]*Log[v]^3)/48 + 
            (655*Log[v]^4)/64)*Log[w]^4 + ((-25*Log[u]^3)/48 + 
            (59*Log[u]^2*Log[v])/16 + (59*Log[u]*Log[v]^2)/16 - 
            (25*Log[v]^3)/48)*Log[w]^5 + ((91*Log[u]^2)/16 - 
            (271*Log[u]*Log[v])/24 + (91*Log[v]^2)/16)*Log[w]^6 + 
          (-Log[u]/16 - Log[v]/16)*Log[w]^7 + (121*Log[w]^8)/96)*zeta[5] + 
        (((569*Log[u]^2)/2 - (2217*Log[u]*Log[v])/4 + (569*Log[v]^2)/2)*
           Log[w]^4 + (-6*Log[u] - 6*Log[v])*Log[w]^5 + 80*Log[w]^6)*
         zeta[7] + 3626*Log[w]^4*zeta[9]) + 
      zeta[2]*(((11*Log[u]^10)/30720 - (43*Log[u]^9*Log[v])/9216 + 
          (23*Log[u]^8*Log[v]^2)/2048 - (23*Log[u]^7*Log[v]^3)/960 + 
          (905*Log[u]^6*Log[v]^4)/27648 - (247*Log[u]^5*Log[v]^5)/6400 + 
          (905*Log[u]^4*Log[v]^6)/27648 - (23*Log[u]^3*Log[v]^7)/960 + 
          (23*Log[u]^2*Log[v]^8)/2048 - (43*Log[u]*Log[v]^9)/9216 + 
          (11*Log[v]^10)/30720)*Log[w]^4 + (-(Log[u]^8*Log[v])/768 + 
          (Log[u]^7*Log[v]^2)/768 - (Log[u]^6*Log[v]^3)/384 + 
          (Log[u]^5*Log[v]^4)/5760 + (Log[u]^4*Log[v]^5)/5760 - 
          (Log[u]^3*Log[v]^6)/384 + (Log[u]^2*Log[v]^7)/768 - 
          (Log[u]*Log[v]^8)/768)*Log[w]^5 + 
        (Log[u]^8/1536 - (5*Log[u]^7*Log[v])/768 + (19*Log[u]^6*Log[v]^2)/
           1536 - (89*Log[u]^5*Log[v]^3)/3840 + (41*Log[u]^4*Log[v]^4)/1728 - 
          (89*Log[u]^3*Log[v]^5)/3840 + (19*Log[u]^2*Log[v]^6)/1536 - 
          (5*Log[u]*Log[v]^7)/768 + Log[v]^8/1536)*Log[w]^6 + 
        (-(Log[u]^6*Log[v])/768 + (Log[u]^5*Log[v]^2)/2304 - 
          (Log[u]^4*Log[v]^3)/768 - (Log[u]^3*Log[v]^4)/768 + 
          (Log[u]^2*Log[v]^5)/2304 - (Log[u]*Log[v]^6)/768)*Log[w]^7 + 
        ((13*Log[u]^6)/18432 - (47*Log[u]^5*Log[v])/9216 + 
          (11*Log[u]^4*Log[v]^2)/1536 - (77*Log[u]^3*Log[v]^3)/6912 + 
          (11*Log[u]^2*Log[v]^4)/1536 - (47*Log[u]*Log[v]^5)/9216 + 
          (13*Log[v]^6)/18432)*Log[w]^8 + (-(Log[u]^4*Log[v])/1536 - 
          (Log[u]^3*Log[v]^2)/4608 - (Log[u]^2*Log[v]^3)/4608 - 
          (Log[u]*Log[v]^4)/1536)*Log[w]^9 + 
        ((7*Log[u]^4)/15360 - (49*Log[u]^3*Log[v])/23040 + 
          (Log[u]^2*Log[v]^2)/512 - (49*Log[u]*Log[v]^3)/23040 + 
          (7*Log[v]^4)/15360)*Log[w]^10 + (-(Log[u]^2*Log[v])/7680 - 
          (Log[u]*Log[v]^2)/7680)*Log[w]^11 + 
        (Log[u]^2/6144 - (17*Log[u]*Log[v])/46080 + Log[v]^2/6144)*
         Log[w]^12 + Log[w]^14/40320 + 
        (((741*Log[u]^4)/128 - (1765*Log[u]^3*Log[v])/96 + 
            (365*Log[u]^2*Log[v]^2)/16 - (1765*Log[u]*Log[v]^3)/96 + 
            (741*Log[v]^4)/128)*Log[w]^4 + ((9*Log[u]^3)/32 + 
            (37*Log[u]^2*Log[v])/32 + (37*Log[u]*Log[v]^2)/32 + 
            (9*Log[v]^3)/32)*Log[w]^5 + ((329*Log[u]^2)/96 - 
            (313*Log[u]*Log[v])/48 + (329*Log[v]^2)/96)*Log[w]^6 + 
          ((3*Log[u])/32 + (3*Log[v])/32)*Log[w]^7 + (151*Log[w]^8)/192)*
         zeta[3]^2 + (((1315*Log[u])/48 + (1315*Log[v])/48)*Log[w]^4 - 
          (31*Log[w]^5)/4)*zeta[3]^3 + 
        (((69*Log[u]^5)/16 - (401*Log[u]^4*Log[v])/96 + 
            (13*Log[u]^3*Log[v]^2)/4 + (13*Log[u]^2*Log[v]^3)/4 - 
            (401*Log[u]*Log[v]^4)/96 + (69*Log[v]^5)/16)*Log[w]^4 + 
          ((-87*Log[u]^4)/32 + (13*Log[u]^3*Log[v])/6 - 
            (51*Log[u]^2*Log[v]^2)/8 + (13*Log[u]*Log[v]^3)/6 - 
            (87*Log[v]^4)/32)*Log[w]^5 + ((437*Log[u]^3)/288 - 
            (19*Log[u]^2*Log[v])/32 - (19*Log[u]*Log[v]^2)/32 + 
            (437*Log[v]^3)/288)*Log[w]^6 + ((-53*Log[u]^2)/48 - 
            (53*Log[v]^2)/48)*Log[w]^7 + ((37*Log[u])/384 + (37*Log[v])/384)*
           Log[w]^8 - Log[w]^9/96)*zeta[5] + (11307*Log[w]^4*zeta[5]^2)/8 + 
        (((2183*Log[u]^3)/24 - (93*Log[u]^2*Log[v])/4 - (93*Log[u]*Log[v]^2)/
             4 + (2183*Log[v]^3)/24)*Log[w]^4 + ((-933*Log[u]^2)/16 - 
            (933*Log[v]^2)/16)*Log[w]^5 + ((29*Log[u])/4 + (29*Log[v])/4)*
           Log[w]^6 - (5*Log[w]^7)/4)*zeta[7] + 
        zeta[3]*(((11*Log[u]^7)/96 - (377*Log[u]^6*Log[v])/1920 + 
            (47*Log[u]^5*Log[v]^2)/192 - (19*Log[u]^4*Log[v]^3)/192 - 
            (19*Log[u]^3*Log[v]^4)/192 + (47*Log[u]^2*Log[v]^5)/192 - 
            (377*Log[u]*Log[v]^6)/1920 + (11*Log[v]^7)/96)*Log[w]^4 + 
          ((-25*Log[u]^6)/384 + (101*Log[u]^5*Log[v])/960 - 
            (109*Log[u]^4*Log[v]^2)/384 + (Log[u]^3*Log[v]^3)/4 - 
            (109*Log[u]^2*Log[v]^4)/384 + (101*Log[u]*Log[v]^5)/960 - 
            (25*Log[v]^6)/384)*Log[w]^5 + ((89*Log[u]^5)/1152 - 
            (101*Log[u]^4*Log[v])/1152 + (31*Log[u]^3*Log[v]^2)/576 + 
            (31*Log[u]^2*Log[v]^3)/576 - (101*Log[u]*Log[v]^4)/1152 + 
            (89*Log[v]^5)/1152)*Log[w]^6 + ((-61*Log[u]^4)/1152 + 
            (Log[u]^3*Log[v])/24 - (Log[u]^2*Log[v]^2)/8 + (Log[u]*Log[v]^3)/
             24 - (61*Log[v]^4)/1152)*Log[w]^7 + 
          ((17*Log[u]^3)/768 - (Log[u]^2*Log[v])/96 - (Log[u]*Log[v]^2)/96 + 
            (17*Log[v]^3)/768)*Log[w]^8 + (-Log[u]^2/64 - Log[v]^2/64)*
           Log[w]^9 + (Log[u]/640 + Log[v]/640)*Log[w]^10 + 
          (((1119*Log[u]^2)/4 - (2071*Log[u]*Log[v])/4 + (1119*Log[v]^2)/4)*
             Log[w]^4 + ((19*Log[u])/2 + (19*Log[v])/2)*Log[w]^5 + 
            (487*Log[w]^6)/6)*zeta[5] + (11695*Log[w]^4*zeta[7])/4) + 
        (((1568*Log[u])/3 + (1568*Log[v])/3)*Log[w]^4 - 98*Log[w]^5)*
         zeta[9]) + ((2205*Log[u])/2 + (2205*Log[v])/2)*Log[w]^4*zeta[11])
